
package com.fjut;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@MapperScan(basePackages = {"com.fjut.server.mapper"})

@EnableScheduling //开启定时任务
@EnableGlobalMethodSecurity(securedEnabled = true)  // 启用security注解
@SpringBootApplication()
public class HomeStaysApplication {
    public static void main(String[] args) {
        SpringApplication.run(HomeStaysApplication.class, args);
    }

    @Bean
    public Queue queue(){
        return new Queue("mail.register");
    }

    /**
     * 修改密码邮箱
     * @return
     */
    @Bean
    public Queue queue1(){
        return new Queue("mail.modifyPassword");
    }

    /**
     * 房东提醒邮箱
     * @return
     */
    @Bean
    public Queue queue2(){
        return new Queue("mail.remindMerchant");
    }
}