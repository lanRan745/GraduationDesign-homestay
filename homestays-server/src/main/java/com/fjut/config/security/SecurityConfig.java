package com.fjut.config.security;


import com.fjut.handler.RestAuthorizationEntryPointHandler;
import com.fjut.handler.RestfulAccessDeniedHandler;
import com.fjut.server.entity.User;
import com.fjut.server.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Security配置类
 * @since 1.0.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private IUserService userService;
    @Autowired
    private RestAuthorizationEntryPointHandler  restAuthorizationEntryPointHandler;
    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;

    /**
     * 指定Security走我们自己的登录逻辑
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }


    /**
     * 根据用户名获取用户
     * UserDetailsService是一个接口并且只有一个方法：loadUserByUsername
     * 在这里实现了这个方法，应该能
     * @return
     */
    @Override
    @Bean
    public UserDetailsService userDetailsService(){
        return username -> {
            User user = userService.getUserByUsername(username);
            return user;
            // 控制台抛出异常
            //throw new UsernameNotFoundException("用户名或密码不正确!!");
        };
    }

    /**
     * 放行接口
     * @param web
     * */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/validateMail",
                "/getHouseDetailMessage",
                "/notifyurl",
                "/userGetNotice",
                "/getHouseIndexMessage",
                "/getComments",
                "/validatePasswordMail",
                "/getLongAndLati",
                "/api/userLogin",
                "/register",
                "/modifyPassword",
                "/logout",
                "/css/**",
                "/js/**",
                "/index.html",
                "favicon.ico",
                "/doc.html",
                "/webjars/**",
                "/swagger-resources/**",
                "/v2/api-docs/**",
                "/captcha",
                "/ws/**"
        );
    }

    /**
     * 自定义登录逻辑
     * @param http
     * @throws Exception
     * */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 自定义登录
        http.formLogin()
                // 当发现/index时认为是登录，必须和表单提交的地址一样，去执行
                .loginProcessingUrl("/api/userLogin");
                // 表单提交，自定义默认登录页面
                //.loginPage("/index");

        // 登录成功后处理器，适用于前后端分离
        // .successHandler(new MyAuthenticationSuccessHandler("/main.html"));
        // .failureHandler(new MyAuthenticationFailureHander("/error.html"));

        // 自定义权限控制
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                // 其他所有请求都必须被认证，必须登录之后被访问
                .anyRequest().authenticated();

        http.csrf()
                .disable()  // 关闭csrf防护，使用JWT
                // 基于token，不需要session
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // 禁用缓存
        http.headers()
                .cacheControl();

        // 添加jwt，登录授权过滤器
        http.addFilterBefore(jwtAuthencationTokenFilter(), UsernamePasswordAuthenticationFilter.class);

        // 添加自定义未授权和未登录返回结果
        http.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthorizationEntryPointHandler);
        // “记住我”功能
//        http.rememberMe()
//                // 失效时间，单位秒
//                .tokenValiditySeconds(300)
//                // 持久层对象
//                .tokenRepository(persistentTokenRepository);
    }



    /**
     * 密码加密
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * 获取用户对象
     * @return
     * */
//    @Bean
//    public PersistentTokenRepository persistentTokenRepository() {
//        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
//        // 引入数据源
//        jdbcTokenRepository.setDataSource(dataSource);
//        // 自动建表，第一次启动时需要，第二次启动注释掉
//        // jdbcTokenRepository.setCreateTableOnStartup(true);
//        return jdbcTokenRepository;
//    }


    /**
     * Jwt登录授权过滤器
     * @return
     */
    @Bean
    public JwtAuthencationTokenFilter jwtAuthencationTokenFilter(){
        return new JwtAuthencationTokenFilter();
    }

}
