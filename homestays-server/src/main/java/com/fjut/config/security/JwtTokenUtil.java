package com.fjut.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * JwtToken工具类
 * */
@Component
public class JwtTokenUtil {
    private static final String CLAIM_KEY_USERNAME="sub";   // 用户名的key
    private static final String CLAIM_KEY_CREATED="created";    // Jwt创建时间
    @Value("${jwt.secret}")
    private String secret;  // Jwt秘钥
    @Value("${jwt.expiration}")
    private Long expiration;    // 失效时间

    /**
     * 根据用户信息生成token
     * @param userDetails
     * @return
     */
    public String generateToken(UserDetails userDetails) {
        Map<String,Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME,userDetails.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(claims);
    }

    /**
     * 从token中获取登录用户名
     * @param token
     * @return
     * */
    public String getUserNameFromToken(String token) {
        String username;

        try {
            // 根据token拿荷载
            Claims claims = getClaimFromToken(token);

            // 拿到用户名
            username = claims.getSubject();
        } catch (Exception e) {
            e.printStackTrace();
            username = null;
        }
        return username;
    }

    /**
     * 验证登录信息是否有效
     * @param token
     * @param userDetails
     * @return
     */
    public boolean validateToken(String token, UserDetails userDetails) {
        // 获取用户名
        String username = getUserNameFromToken(token);
        // 判断用户名是否相等 && token是否失效
        return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    /**
     * 判断token是否失效
     *
     * @param token
     * @return
     * */
    private Boolean isTokenExpired(String token) {
        // 获取失效时间
        Date expirDate = getExpireDateFromToken(token);
        return expirDate.before(new Date());
    }

    /**
     * 获取token失效时间
     *
     * @param token
     * @return*/
    private Date getExpireDateFromToken(String token) {
        Claims claims = getClaimFromToken(token);
        return claims.getExpiration();
    }

    /**
     * 砍断token是否可以被刷新
     * @param token
     * @return
     */
    public boolean canRefresh(String token) {
        return !isTokenExpired(token);
    }

    /**
     * 刷新token
     * @param token
     * @return
     */
    public String refreshToken(String token) {
        Claims claims = getClaimFromToken(token);
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }

    /**
     * 从token中获取荷载
     * @param token
     * @return
     * */
    private Claims getClaimFromToken(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)  // 秘钥
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return claims;
    }

    /**
     * 根据荷载生成JWT Token
     *
     * @param claims
     * @return
     * */
    private String generateToken(Map<String,Object> claims) {
        return Jwts.builder()
                .setClaims(claims)  // 荷载
                .setExpiration(generateExpirationDate())    // 失效时间
                .signWith(SignatureAlgorithm.HS512,secret) // 签名
                .compact();// 秘钥
    }

    /**
     * 生成token失效时间
     * @return
     * */
    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration*1000);
    }
}