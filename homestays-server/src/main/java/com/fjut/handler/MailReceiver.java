package com.fjut.handler;


import com.fjut.server.entity.Order;
import com.fjut.server.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Map;


/**
 * @Author lanRan
 * @Date 2021/4/21 3:16
 * @Version 1.0
 **/
@Component
public class MailReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailReceiver.class);

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private MailProperties mailProperties;
    @Autowired
    private TemplateEngine templateEngine;

    @RabbitListener(queues = "mail.register")
    public void handler(Map map){
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg);

        String email = map.get("email").toString();
        String code  = map.get("code").toString();

        System.out.println(email);
        System.out.println(code);
        try{
            //发件人
            helper.setFrom(mailProperties.getUsername());
            //收件人
            helper.setTo(email);
            //主题
            helper.setSubject("用户邮箱验证");
            //发送日期
            helper.setSentDate(new Date());
            //内容
            Context context = new Context();
            //邮件内容
            context.setVariable("name", "亲爱的用户");
            context.setVariable("code", code);
            //将mail.html文件和值设置进去
            String mail = templateEngine.process("mail", context);
            helper.setText(mail,true);
            //发送邮件
            javaMailSender.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("邮件发送失败",e.getMessage());
        }
    }

    @RabbitListener(queues = "mail.modifyPassword")
    public void handler1(Map map){
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg);

        String email = map.get("email").toString();
        String code  = map.get("code").toString();
        //String username = map.get("username").toString();

        System.out.println(email);
        System.out.println(code);
        try{
            //发件人
            helper.setFrom(mailProperties.getUsername());
            //收件人
            helper.setTo(email);
            //主题
            helper.setSubject("修改密码验证");
            //发送日期
            helper.setSentDate(new Date());
            //内容
            Context context = new Context();
            //邮件内容
            context.setVariable("name", "亲爱的用户");
            context.setVariable("code", code);
            //将mail.html文件和值设置进去
            String mail = templateEngine.process("modifyMail", context);
            helper.setText(mail,true);
            //发送邮件
            javaMailSender.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("邮件发送失败",e.getMessage());
        }
    }
    @RabbitListener(queues = "mail.remindMerchant")
    public void handler2(Map<String,Object> map){
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg);

        Order order = (Order) map.get("order");
        User user = (User) map.get("user");
        String time = order.getCreateTime().toString();
        String arriveDate = order.getArriveDate().toString();
        String leaveDate = order.getLeaveDate().toString();
        String email = user.getEmail();
        String username = user.getUsername();
        try{
            //发件人
            helper.setFrom(mailProperties.getUsername());
            //helper.setFrom("Airnb团队");
            //收件人
            helper.setTo(email);
            //主题
            helper.setSubject("新订单确认提醒");
            //发送日期
            helper.setSentDate(new Date());
            //内容
            Context context = new Context();
            //邮件内容
            context.setVariable("name", "亲爱的" + username);
            context.setVariable("time", time);
            context.setVariable("arriveDate", arriveDate);
            context.setVariable("leaveDate", leaveDate);
            //将mail.html文件和值设置进去
            String mail = templateEngine.process("remindMerchant", context);
            helper.setText(mail,true);
            //发送邮件
            javaMailSender.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("邮件发送失败",e.getMessage());
        }
    }
}
