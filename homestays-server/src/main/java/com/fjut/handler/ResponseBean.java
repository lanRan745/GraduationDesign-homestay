package com.fjut.handler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 公共返回对象
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseBean {

    private long code;
    private String msg;
    private Object obj;

    /**
     * 成功返回结果
     * @param msg
     * @return
     */
    public static ResponseBean success(String msg) {
        return new ResponseBean(201,msg,null);
    }

    /**
     * 成功返回结果的重载方法
     * @param msg
     * @param obj
     * @return
     */
    public static ResponseBean success(String msg,Object obj) {
        return new ResponseBean(201,msg,obj);
    }

    /**
     * 成功返回结果的重载方法
     * @param code
     * @param msg
     * @param obj
     * @return
     */
    public static ResponseBean success(long code,String msg,Object obj) {
        return new ResponseBean(code,msg,obj);
    }

    /**
     * 失败返回结果
     * @param msg
     * @return
     */
    public static ResponseBean error(String msg) {
        return new ResponseBean(500,msg,null);
    }

    /**
     * 失败返回结果的重载方法
     * @param msg
     * @param obj
     * @return
     */
    public static ResponseBean error(String msg,Object obj) {
        return new ResponseBean(500,msg,obj);
    }

    /**
     * 失败返回结果的重载方法
     * @param code
     * @param msg
     * @param obj
     * @return
     */
    public static ResponseBean error(long code,String msg,Object obj) {
        return new ResponseBean(code,msg,obj);
    }
    public static ResponseBean error(long code,String msg) {
        return new ResponseBean(code,msg,null);
    }

}
