package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fjut.server.entity.Notice;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-06-05
 */
public interface NoticeMapper extends BaseMapper<Notice> {

    IPage<Notice> noticeSearch(Page<Notice> page, @Param("title")String title, @Param("role")String role);
}
