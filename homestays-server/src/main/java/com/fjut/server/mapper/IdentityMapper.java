package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fjut.server.entity.Identity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-30
 */
public interface IdentityMapper extends BaseMapper<Identity> {

}
