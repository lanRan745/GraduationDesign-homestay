package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fjut.server.entity.AliPayBean;

public interface AliPayBeanMapper extends BaseMapper<AliPayBean> {
}
