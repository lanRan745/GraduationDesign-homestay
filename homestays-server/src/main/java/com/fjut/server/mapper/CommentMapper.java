package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fjut.server.entity.Comment;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
public interface CommentMapper extends BaseMapper<Comment> {

    IPage<Comment> getCommentsPage(Page<Comment> page, @Param("id")Integer houseId);

    Double getRating(@Param("id")Integer houseId);
}
