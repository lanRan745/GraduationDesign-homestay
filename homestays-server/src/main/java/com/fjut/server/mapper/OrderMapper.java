package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-28
 */
public interface OrderMapper extends BaseMapper<Order> {
    List<Order> notCancelOrders(@Param("houseId")Integer houseId);
    Integer updatePaymentOrder(@Param("no")String no);
    Order selectOrderByNo(@Param("no")String no);
    Order getNeedRemindOrder(@Param("no")String no);

    //获取即将开始的旅程订单
    IPage<Order> getAheadUserOrders(Page<Order> page,@Param("userId")Integer userId);
    //获取过往的订单
    IPage<Order> getPastUserOrders(Page<Order> page,@Param("userId")Integer userId);

    IPage<Order> getCheckOrders(Page<Order> page, @Param("merchantId")Integer merchantId);

    IPage<Order> searchCheckOrders(Page<Order> page, @Param("merchantId") Integer merchantId, @Param("username") String username, @Param("email") String email,  @Param("orderStatus")Integer orderStatus, @Param("checkStatus")Integer checkStatus, @Param("no") String no);
    IPage<Order> getMerchantAllOrders(Page<Order> page, @Param("merchantId") Integer merchantId, @Param("username") String username, @Param("email") String email,  @Param("orderStatus")Integer orderStatus, @Param("no") String no);

    List<Order> getOneMonthOrders( @Param("merchantId")Integer merchantId);

}
