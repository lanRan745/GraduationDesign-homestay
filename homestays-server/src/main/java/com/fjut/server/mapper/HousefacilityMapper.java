package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fjut.server.entity.Housefacility;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-27
 */
public interface HousefacilityMapper extends BaseMapper<Housefacility> {

}
