package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fjut.server.entity.House;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
public interface HouseMapper extends BaseMapper<House> {

}
