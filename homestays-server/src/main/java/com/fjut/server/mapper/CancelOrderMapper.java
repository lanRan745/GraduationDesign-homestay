package com.fjut.server.mapper;

import com.fjut.server.entity.CancelOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-28
 */
public interface CancelOrderMapper extends BaseMapper<CancelOrder> {

}
