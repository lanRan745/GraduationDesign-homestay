package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Facility;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-27
 */
public interface FacilityMapper extends BaseMapper<Facility> {

}
