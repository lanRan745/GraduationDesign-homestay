package com.fjut.server.mapper;

import com.fjut.server.entity.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-24
 */
public interface LogMapper extends BaseMapper<Log> {

}
