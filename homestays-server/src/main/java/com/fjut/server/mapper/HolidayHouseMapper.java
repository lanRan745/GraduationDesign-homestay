package com.fjut.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fjut.server.entity.HolidayHouse;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
public interface HolidayHouseMapper extends BaseMapper<HolidayHouse> {

    IPage verifyHolidayHouses(Page<HolidayHouse> page);
}
