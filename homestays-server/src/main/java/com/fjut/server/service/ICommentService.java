package com.fjut.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Comment;

import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
public interface ICommentService extends IService<Comment> {

    ResponseBean getCommentAmount();

    ResponseBean getComments(Integer currentPage, Integer size, Integer houseId);

    ResponseBean addComment(Integer rating, String content, Integer orderId, Integer userId, Integer houseId) throws IOException;
}
