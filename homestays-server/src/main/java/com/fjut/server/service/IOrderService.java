package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.AliPayBean;
import com.fjut.server.entity.Order;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-28
 */
public interface IOrderService extends IService<Order> {
    ResponseBean notCancelOrders(Integer houseId);

    ResponseBean userOrderPay(Map<String, String> map, String total_amount);

    ResponseBean getPastUserOrders(Integer currentPage, Integer size);

    ResponseBean getAheadUserOrders(Integer currentPage, Integer size);

    ResponseBean payOrderAgain(AliPayBean aliPayBean, Integer id);

    ResponseBean userCancelOfOrder(Map<String, String> map, Integer id);

    ResponseBean getCheckOrders(Integer currentPage, Integer size);

    ResponseBean determineOrder(Integer id);

    ResponseBean searchCheckOrders(Integer currentPage, Integer size, String username, String email, Integer orderStatus, Integer checkStatus, String no);

    ResponseBean merchantCancelOrder(Integer orderId, String remarks, String no, Integer dayAmount);

    ResponseBean getMerchantAllOrders(Integer currentPage, Integer size, String username, String email, Integer orderStatus, String no);

    ResponseBean getOrderStatistics();
}

