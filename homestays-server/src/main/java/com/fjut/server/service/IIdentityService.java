package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Identity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-30
 */
public interface IIdentityService extends IService<Identity> {

    ResponseBean addUserIdentity(List<Identity> identityList, Integer orderId, String username, String idNumber);
}
