package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Identity;
import com.fjut.server.entity.Order;
import com.fjut.server.mapper.IdentityMapper;
import com.fjut.server.mapper.OrderMapper;
import com.fjut.server.service.IIdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-30
 */
@Service
@Primary
public class IdentityServiceImpl extends ServiceImpl<IdentityMapper, Identity> implements IIdentityService {

    @Autowired
    private IdentityMapper identityMapper;
    @Autowired
    private OrderMapper orderMapper;

    @Transactional
    @Override
    public ResponseBean addUserIdentity(List<Identity> identityList, Integer orderId, String username, String idNumber) {

        Identity i = new Identity();
        i.setIdNumber(idNumber);
        i.setOrderId(orderId);
        i.setUsername(username);
        identityList.add(i);

        Map<String,Integer> map = new HashMap<>();
        for(Identity identity : identityList){
            if(!map.containsKey(identity.getIdNumber())){
                identity.setOrderId(orderId);
                identityMapper.insert(identity);
                map.put(identity.getIdNumber(), 1);
            } else {
                return new ResponseBean().error("存在重复的身份证号码，请检查后重新输入");
            }
        }

        //修改入住状态 1 -> 0 表示入住
        Order order = new Order();
        order.setCheckStatus(0);
        order.setId(orderId);

        return orderMapper.updateById(order) > 0 ?  new ResponseBean().success("成功入住") : new ResponseBean().error("发生错误，请重试");
    }
}
