package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Notice;
import com.fjut.server.mapper.NoticeMapper;
import com.fjut.server.service.INoticeService;
import com.fjut.util.SystemTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-06-05
 */
@Service
@Primary
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public ResponseBean noticeSearch(String title, String role, Integer currentPage, Integer size) {

        Page<Notice> page = new Page<>(currentPage,size);
        IPage<Notice> Ipage = noticeMapper.noticeSearch(page,title,role);
        Map<String,Object> map = new HashMap<>();
        map.put("total",Ipage.getTotal());
        map.put("records",Ipage.getRecords());
        return new ResponseBean().success("获取公告信息",map);
    }

    @Override
    public ResponseBean deleteNotice(Integer id) {
        Notice notice = new Notice();
        notice.setId(id);
        notice.setIsDelete(0);
        return noticeMapper.updateById(notice) > 0 ? new ResponseBean().success("删除成功") : new ResponseBean().error("删除失败，请重试");
    }

    @Override
    public ResponseBean addNotice(Notice notice) {
        notice.setCreateTime(SystemTimeUtil.getCurrentTime());
        return noticeMapper.insert(notice) > 0 ? new ResponseBean().success("添加成功") : new ResponseBean().error("添加失败，请重试");
    }

    @Override
    public ResponseBean userGetNotice() {
        return new ResponseBean().success("获得用户公告",noticeMapper.selectList(new QueryWrapper<Notice>().eq("role", "user")));
    }

    @Override
    public ResponseBean merchantGetNotice() {
        return new ResponseBean().success("获得房东公告",noticeMapper.selectList(new QueryWrapper<Notice>().eq("role", "merchant")));
    }
}
