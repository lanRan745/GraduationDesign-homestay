package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Log;
import com.fjut.server.entity.User;
import com.fjut.server.mapper.LogMapper;
import com.fjut.server.mapper.UserMapper;
import com.fjut.server.service.ILogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-24
 */
@Service
@Primary
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements ILogService {

    @Autowired
    private LogMapper logMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    UserMapper userMapper;

    @Override
    public ResponseBean getLogByMerchantId() {
        Integer id = getUserId();
        List<Log> logs = logMapper.selectList(new QueryWrapper<Log>().eq("userId", id).orderByDesc( "isConsult","createTime"));

        return new ResponseBean().success("221",logs);
    }

    /**
     * 消除红点
     * @param id
     * @return
     */
    @Override
    public ResponseBean clearRed(Integer id) {

        logMapper.update(null,new UpdateWrapper<Log>().eq("id",id).set("isConsult",0));

        return new ResponseBean().success("成功消除红点！");
    }

    // 获得商家id
    public Integer getUserId() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }

        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }

        Integer merchantId = user.getId();

        return merchantId;
    }
}
