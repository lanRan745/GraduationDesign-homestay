package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.HolidayHouse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-23
 */
public interface IHolidayHouseService extends IService<HolidayHouse> {

    ResponseBean addHolidayHouse(String lng, String lat, String bdAddress, String address, String province, String city, String area, String describe, String detailDescribe, MultipartFile[] files) throws IOException;

    ResponseBean scanHomestays();

    ResponseBean modifyAuditStatus(Integer id, Integer auditStatus);

    ResponseBean modifyHomestays(String describe, String detailDescribe, String id, Integer auditStatus, MultipartFile[] files) throws IOException;

    ResponseBean getLongAndLati(String lng, String lat, Integer buttonStatus, Integer min, Integer max, Integer iscancel);

    ResponseBean verifyHolidayHouses(Integer currentPage, Integer size);

    ResponseBean rejectHolidayHouse(Integer id, Integer merchantId, String address, String remark);

    ResponseBean passHolidayHouse(Integer id, Integer merchantId, String address);

    ResponseBean getAdminIndexData();
}
