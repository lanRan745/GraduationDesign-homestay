package com.fjut.server.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.server.entity.AliPayBean;

public interface IAliPayBeanService extends IService<AliPayBean> {
}
