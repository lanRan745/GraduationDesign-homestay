package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.server.entity.CancelOrder;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-28
 */
public interface ICancelOrderService extends IService<CancelOrder> {

}
