package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.config.security.JwtTokenUtil;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.HolidayHouse;
import com.fjut.server.entity.House;
import com.fjut.server.entity.Order;
import com.fjut.server.entity.User;
import com.fjut.server.mapper.HolidayHouseMapper;
import com.fjut.server.mapper.HouseMapper;
import com.fjut.server.mapper.OrderMapper;
import com.fjut.server.mapper.UserMapper;
import com.fjut.server.service.IUserService;
import com.fjut.util.OSSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-05
 */
@Service
@Primary
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private PasswordEncoder passwordEncoder; //密码加密
    @Autowired
    private JwtTokenUtil jwtUtil;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private HolidayHouseMapper holidayHouseMapper;
    @Autowired
    private HouseMapper houseMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OSSUtil ossUtil;

    /**
     * 登录之后返回token
     *
     * @param username
     * @param password
     * @param role
     * @param request
     * @param response
     * @return
     */
    @Override
    public ResponseBean login(String username, String password, String validationCode, String role, HttpServletRequest request, HttpServletResponse response) {
        // 这里是存储密码的时候进行编译
        // String pass = passwordEncoder.encode("123");

        // 拿到图形验证码
        String captcha = (String)request.getSession().getAttribute("captcha");
        System.out.println("============");
        System.out.println(validationCode);
        System.out.println(captcha);


        if (validationCode == null || !validationCode.equalsIgnoreCase(captcha)){
            return ResponseBean.error(410,"验证码输入错误,请重新输入");
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        if(userDetails == null || !passwordEncoder.matches(password, userDetails.getPassword())){
            System.out.println("------------------------");

            return ResponseBean.error("用户名或密码不正确");
        }

        if(!userDetails.isEnabled()){
            return ResponseBean.error("账号被禁用，请联系管理员！");
        }
        //获得当前账号的角色
        String accountRole = userMapper.selectOne(new QueryWrapper<User>().eq("username",username)).getAccountRole();
        System.out.println("role:" + role);
        System.out.println("accountRole:" + accountRole);
        if(!handleRole(role).equals(accountRole)){
            return ResponseBean.error("用户名或密码不正确");
        }

        //更新security登录用户对象
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails
        ,null,userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        //生成 token和头部信息
        String token = jwtUtil.generateToken(userDetails);
        //封装响应数据
        Map<String,String> tokenMap = new HashMap<>();
        tokenMap.put("token",token);
        tokenMap.put("tokenHead", tokenHead);
        System.out.println(token);
        //返回用户信息
        tokenMap.put("username", userDetails.getUsername());
        String uuid = UUID.randomUUID().toString();
        tokenMap.put("uuid",uuid);
        tokenMap.put("role",accountRole);

        return ResponseBean.success(200,"登录成功",tokenMap);
    }

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return
     */
    @Override
    public User getUserByUsername(String username) {
        return userMapper.selectOne(new QueryWrapper<User>().eq("username", username));
    }

    /**
     * 通过邮箱和角色信息获取用户个人信息
     * @param email
     * @param role
     * @return
     */
    @Override
    public User getUserByEmail(String email, String role) {
        return userMapper.selectOne(new QueryWrapper<User>().eq("accountRole", role).eq("email", email));
    }

    /**
     * 测试：获得所有用户信息
     * @return
     */
    @Override
    public List<User> getAllUserInfo() {
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        // 从redis获取用户数据 在对用户进行增删改操作的同时，要将redis全部用户信息删除
        List<User> users = (List<User>) valueOperations.get("AllUserInfo");
        System.out.println(users);
        // 如果为空，去数据库获取
        if (CollectionUtils.isEmpty(users)) {
            // 查询参数为null表示查询全部
            users = userMapper.selectList(null);
            // 将数据设置进redis
            valueOperations.set("AllUserInfo", users);
        }
        return users;
    }

    /**
     * 注册
     * @param user
     * @return
     */
    @Override
    @Transactional
    public ResponseBean register(User user) {

        user.setAccountRole(handleRole(user.getAccountRole()));

        if(userMapper.selectOne(new QueryWrapper<User>().eq("username",user.getUsername())) != null){
            //System.out.println("用户名已被注册，请修改用户名");
            return new ResponseBean().error("用户名已被注册，请修改用户名");
        }

        // 判断是否已经注册过
        if(userMapper.selectOne(new QueryWrapper<User>().eq("accountRole",user.getAccountRole()).eq("email",user.getEmail())) != null) {
            //System.out.println("邮箱已被注册，请绑定其他邮箱");
            return new ResponseBean().error("邮箱已被注册，请绑定其他邮箱");

        } else if (user.getTelephone() != null && userMapper.selectOne(new QueryWrapper<User>().eq("accountRole",user.getAccountRole()).eq("telephone", user.getTelephone())) != null) {
            //System.out.println("电话号码已被注册，请绑定其他电话号码");
            return new ResponseBean().error("电话号码已被注册，请绑定其他电话号码");
        }

        // 对原密码进行加密处理
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // 对年龄进行处理
        if(user.getIdNumber() != null) {
            user.setAge(handleAge(user.getIdNumber()));
        }

        System.out.println("处理后的数据为：" + user.toString());

        int result = userMapper.insert(user);
        System.out.println(result);

       return result > 0 ? new ResponseBean().success("注册成功") : new ResponseBean().error("注册失败，请重试");
    }

    /**
     * 修改密码
     * @param role
     * @param password
     * @param email
     * @return
     */
    @Override
    public ResponseBean modifyPassword(String role, String password, String email) {
        //提取和转换用户身份
        if(role.equals("房东")) role = "merchant";
        else if(role.equals("用户")) role = "user";
        else role = "admin";
        //对密码加密
        password = passwordEncoder.encode(password);

        if(userMapper.selectOne(new QueryWrapper<User>().eq("accountRole",role).eq("email",email)) == null) {
            System.out.println("该邮箱未注册，无法修改！");
            return new ResponseBean().error("该邮箱未注册，无法修改！");
        } else {

            int result = userMapper.update(null,new UpdateWrapper<User>().eq("accountRole",role).eq("email",email).set("password",password));
            System.out.println("修改中");
        return result > 0 ? new ResponseBean().success("密码修改成功！") : new ResponseBean().error("修改失败！请重试");
        }
        //return null;
    }

    /**
     * 获取当前用户信息
     * @return
     */
    @Override
    public ResponseBean getSingleUserInfo() {
        User user = getUser();
        Integer merchantId = user.getId();
        Map<String,Object> map = new HashMap<>();

        map.put("username",user.getUsername());
        map.put("realname",user.getRealname());
        map.put("telephone",user.getTelephone());
        map.put("isEnabled",user.getIsEnabled());
        map.put("email",user.getEmail());
        map.put("idNumber",user.getIdNumber());
        map.put("age",user.getAge());

        //订单
        List<Order> orders = orderMapper.selectList(new QueryWrapper<Order>().eq("merchantId", merchantId));
        // 拥有的订单数量
        map.put("totalOrdersNumber",orders.size());
        List<HolidayHouse> holidayHouses = holidayHouseMapper.selectList(new QueryWrapper<HolidayHouse>().eq("merchantId", merchantId));
        // 拥有的房源数量
        map.put("totalHolidayHouseNumber",holidayHouses.size());
        // 目前正在营运的数量
        List<House> houses = houseMapper.selectList(new QueryWrapper<House>().eq("merchantId",merchantId));
        map.put("totalOnBusiness_2HouseNumber",houses.size());

        Integer totalPrice = 0;

        int overOrdersNumber = 0;
        int cancelOrdersNumber = 0;
        int yuOrdersNumber = 0;
        // 总共获利
//        for(Order o : orders){
//            int state = o.getOrderStatus();
//            if( state == 0 ){
//                cancelOrdersNumber++;
//            }
//            else if( state == 1 ){
//                yuOrdersNumber++;
//            }
//            else if( state == 2 || state == 3 ){
//                totalPrice += o.getPrice();
//                overOrdersNumber ++;
//            }
//        }
        map.put("overOrdersNumber",overOrdersNumber);
        map.put("cancelOrdersNumber",cancelOrdersNumber);
        map.put("yuOrdersNumber",yuOrdersNumber);
        map.put("totalPrice",totalPrice);

        return new ResponseBean().success("获取成功",map);
    }

    @Override
    public ResponseBean getSimpleUserInfo() {
        User user = getUser();
        return new ResponseBean().success("get!",user);
    }

    /**
     * 修改用户信息
     * @param
     * @return
     */
    @Override
    public ResponseBean modifyInfo(String realname,String idNumber,String telephone) {
        System.out.println("realname:"+realname);
        System.out.println("telephone:"+telephone);
        System.out.println("idNumber:"+idNumber);
        User u = getUser();
        Integer age = null;
        if(("".equals(realname) || realname == null) && ("".equals(telephone) || telephone == null) && ( "".equals(idNumber) || idNumber == null))
            return new ResponseBean().success("未做出修改");

        if(idNumber != null && !idNumber.equals(""))
            if(!idNumber.equals(u.getIdNumber()))
                age = handleAge(idNumber);

        redisTemplate.delete("user-" + u.getUsername());
        int result =
                userMapper.update(null,new UpdateWrapper<User>()
                        .eq("username", u.getUsername())
                        .set(realname!="" || realname != null,"realname", realname)
                        .set(idNumber!="" || idNumber != null,"idNumber",  idNumber)
                        .set(telephone!="" || telephone != null, "telephone", telephone)
                        .set(age!=null,"age",age));
        return result > 0 ? new ResponseBean().success("修改成功") : new ResponseBean().error("修改失败，请重试");
    }

    //更新头像
    @Override
    public ResponseBean updateAvatar(MultipartFile file) throws IOException {
        String url = null;
        if(file != null){
            url = ossUtil.uploadFile(file, "用户头像");
        }
        User u = getUser();
        int r = userMapper.update(null, new UpdateWrapper<User>()
                .eq("username", u.getUsername())
                .set(url != null, "avatar", url));
        redisTemplate.delete("user-"+u.getUsername());
        return r > 0 ? new ResponseBean().success("修改成功！",url) : new ResponseBean().error("更新头像错误，请重试！");
    }

    @Override
    public ResponseBean userModifyPassword(Integer userId, String newp, String oldp) {

        User user = getUser();
        if(!!passwordEncoder.matches(oldp,user.getPassword())){
            return new ResponseBean().error("原密码出错，请重新输入");
        }

        User u = new User();
        u.setId(user.getId());
        u.setPassword(passwordEncoder.encode(newp));
        int result = userMapper.updateById(u);

        if(result > 0){
            //删除redis中的数据
            redisTemplate.delete("user-"+user.getUsername());
            return new ResponseBean().success("修改成功！");
        }
        return new ResponseBean().error("修改错误，请重试");
    }

    @Override
    public ResponseBean powerSearch(String username, String role, Integer currentPage, Integer size) {

        Page<User> page = new Page<>(currentPage,size);
        IPage<User> Ipage = userMapper.powerSearch(page,username,role);
        Map<String,Object> map = new HashMap<>();
        map.put("total",Ipage.getTotal());
        map.put("records",Ipage.getRecords());
        return new ResponseBean().success("获取用户信息",map);
    }

    @Override
    public ResponseBean ban(Integer id) {
        User user = new User();
        user.setId(id);
        user.setIsEnabled(0);
        if(userMapper.updateById(user) > 0)
            return new ResponseBean().success("禁用成功");
        return new ResponseBean().error("禁用失败");
    }

    @Override
    public ResponseBean recovery(Integer id) {
        User user = new User();
        user.setId(id);
        user.setIsEnabled(1);
        if(userMapper.updateById(user) > 0)
            return new ResponseBean().success("恢复成功");
        return new ResponseBean().error("恢复失败");
    }

    public String handleRole(String role){
        //提取和转换用户身份
        if(role.equals("房东")) role = "merchant";
        else if(role.equals("用户")) role = "user";
        else role = "admin";
        return role;
    }

    //处理年龄
    public int handleAge (String id){
        String year = id.substring(6,10);
        Calendar calendar = Calendar.getInstance();
        int nowYear = calendar.get(Calendar.YEAR);
        int  age = nowYear - Integer.parseInt(year) -1;
        return age;
    }

    // 获得商家id
    public Integer getUserId() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }

        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        Integer merchantId = user.getId();
        return merchantId;
    }

    // 获得商家
    public User getUser() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }

        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        return user;
    }
}
