package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Log;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-24
 */
public interface ILogService extends IService<Log> {

    ResponseBean getLogByMerchantId();

    ResponseBean clearRed(Integer id);
}
