package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Notice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-06-05
 */
public interface INoticeService extends IService<Notice> {

    ResponseBean noticeSearch(String title, String role, Integer currentPage, Integer size);

    ResponseBean deleteNotice(Integer id);

    ResponseBean addNotice(Notice notice);

    ResponseBean userGetNotice();

    ResponseBean merchantGetNotice();
}
