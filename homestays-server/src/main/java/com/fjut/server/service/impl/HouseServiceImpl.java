package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.*;
import com.fjut.server.mapper.*;
import com.fjut.server.service.IHouseService;
import com.fjut.util.LocationUtil;
import com.fjut.util.OSSUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
@Service
@Primary
public class HouseServiceImpl extends ServiceImpl<HouseMapper, House> implements IHouseService {

    @Autowired
    private HouseMapper houseMapper;
    @Autowired
    private HolidayHouseMapper holidayHouseMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HousefacilityMapper housefacilityMapper;
    @Autowired
    private FacilityMapper facilityMapper;
    @Autowired
    private OSSUtil ossUtil;
    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Override
    @Transactional
    public ResponseBean addNewHouse(Map<String,Object> map, MultipartFile[] files) throws IOException {

        Integer merchantId = getUserId();
        Integer holidayHouseId =Integer.valueOf((String)map.get("holidayHouseId"));

        //处理逻辑业务
        //1. 先判断该房间类型，如果是整套，只能添加一次，否则直接添加
        Integer houseType = Integer.valueOf((String)map.get("houseType"));
        if(houseType == 1){ // 该房型为整套
            //从数据库中获取该房型是否还有整套的
            House house = houseMapper.selectOne(new QueryWrapper<House>()
                            .eq("merchantId", merchantId)
                            .eq("holidayHouseId", holidayHouseId)
                            .eq("houseType", 1));
            if(house != null){
                return new ResponseBean().error("已经有过该房源的整套出租类型，无法再次添加，您可以尝试添加单间房型");
            }
        }

        House house = new House();
        house.setIsCancel(Integer.valueOf((String)map.get("isCancel")));
        house.setHouseTitle((String)map.get("title"));
        house.setMerchantId(merchantId);
        house.setHolidayHouseId(holidayHouseId);

        String arriveTime = (String) map.get("arriveTime")+":00";
        String leaveTime = (String) map.get("leaveTime")+":00";

        SimpleDateFormat format= new SimpleDateFormat("HH:mm:ss");
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(arriveTime);
            d2 = format.parse(leaveTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Time t1 = new Time(d1.getTime()).valueOf(arriveTime);
        Time t2 = new Time(d2.getTime()).valueOf(leaveTime);

        house.setArriveTime(t1);
        house.setLeaveTime(t2);
        house.setHouseDescribe((String)map.get("houseDescribe"));
        house.setPattern((String)map.get("pattern"));

        house.setDiscount(Double.valueOf((String)map.get("discount")));
        house.setPrice(Double.valueOf((String)map.get("price")));
        house.setHouseType(Integer.valueOf((String)map.get("houseType")));

        if(files == null){
            return new ResponseBean().error("图片不能为空");
        }

        // 获得图片路径
        StringBuffer url = new StringBuffer();

        for(int i=0; i < files.length ;i++){
            if(i==0){
                url.append(ossUtil.uploadFile(files[i],"相关证件"));
            }else{
                url.append(";"+ossUtil.uploadFile(files[i],"相关证件"));
            }
        }
        house.setHouseImages(url.toString());

        String f = (String)map.get("facilities");
        String[] fs = f.split(",");
        List<Integer> facilities = new ArrayList<>();
        for(String ff : fs){
            facilities.add(Integer.valueOf(ff));
        }

        // 房间为单间 或者 没有添加过 整套房型
        int result = houseMapper.insert(house);
        if(result == 0){
            return new ResponseBean().error("插入失败，请重试");
        }

        Integer houseId = house.getId();

        Housefacility housefacility = new Housefacility();
        housefacility.setHouseId(houseId);
        int i = 0;
        //存入houseFacility
        for(Integer fff : facilities) {

            housefacility.setFacilityId(fff);
            i += housefacilityMapper.insert(housefacility);
        }
        if(i == facilities.size())
            return new ResponseBean().success("成功添加！");

        return new ResponseBean().error("添加失败，请重试");
    }


    /**
     * 获取房源下所有租赁信息
     * @param holidayHouseId
     * @return
     */
    @Override

    public ResponseBean getHouses(Integer holidayHouseId) {

        Integer merchantId = getUserId();
        List<House> houses = houseMapper.selectList(new QueryWrapper<House>()
                                            .eq("merchantId", merchantId)
                                            .eq("holidayHouseId", holidayHouseId));

        for (House h : houses){
            Integer houseId = h.getId();
            // 获取到所有图标中间消息
            List<Housefacility> housefacilities = housefacilityMapper.selectList(new QueryWrapper<Housefacility>().eq("houseId",houseId));
            List<Facility> f = new ArrayList<>();
            for(Housefacility hf : housefacilities){
                f.add(facilityMapper.selectById(hf.getFacilityId()));
            }
            h.setHouseFacilities(f);
            h.toString();
        }

        for (int i = 0; i < houses.size(); i++) {
            House h = houses.get(i);
            String AllUrl = h.getHouseImages();
            String Urls[] = AllUrl.split(";");
            List<String> l = new ArrayList();
            for (String url :  Urls) {
                l.add(url);
            }
            h.setAllDetailImages(l);
        }


        return new ResponseBean().success("获取所有房间信息",houses);
    }


    /**
     * 删除房间信息
     * @param houseId
     * @return
     */
    @Override
    public ResponseBean deleteHouse(Integer houseId) {

        //先判断该房间是否有订单
        List<Order> order = orderMapper.selectList(new QueryWrapper<Order>().eq("houseId", houseId));
        if(order.size() > 0)
            return ResponseBean.error("删除失败，该房间已经有"+ order.size() +"个订单产生，无法删除");

//        Map<String,Object> map = new HashMap<>();
//        map.put("houseId", houseId);

        int result = houseMapper.deleteById(houseId);

        if(result > 0)
            return new ResponseBean().success("删除房间成功");

        return new ResponseBean().error("删除房间失败");
    }


    /**
     * 修改房间营业状态
     * @param houseId
     * @param businessStatus
     * @return
     */
    @Override
    public ResponseBean modifyBusinessStatus(Integer houseId, Integer businessStatus) {
        House house = new House();
        house.setId(houseId);
        house.setBusinessStatus(businessStatus);

        int result = houseMapper.updateById(house);
        if(result > 0)
            return new ResponseBean().success("修改状态成功");
        return new ResponseBean().error("修改状态失败");
    }

    @Override
    public ResponseBean getHouseDetailMessage(Integer houseId) {

        Map<String,Object> map = new HashMap<>();
        //服务信息关联

        //房间信息
        House house = houseMapper.selectById(houseId);
        //处理图片
        String[] urls = house.getHouseImages().split(";");
        house.setAllDetailImages(Arrays.asList(urls));
        map.put("houseInfo",house);

        //房东信息
        Integer merchantId = house.getMerchantId();
        User merchantInfo = userMapper.selectById(merchantId);
        map.put("merchantInfo",merchantInfo);

        //房源信息
        Integer holidayHouseId = house.getHolidayHouseId();
        HolidayHouse holidayHouseInfo = holidayHouseMapper.selectById(holidayHouseId);
        map.put("holidayHouseInfo",holidayHouseInfo);

        //房间提供的设施
        // 获取到所有图标中间消息
        List<Housefacility> housefacilities = housefacilityMapper.selectList(new QueryWrapper<Housefacility>().eq("houseId",houseId));
        List<Facility> facilities = new ArrayList<>();
        for(Housefacility hf : housefacilities)
            facilities.add(facilityMapper.selectById(hf.getFacilityId()));
        house.setHouseFacilities(facilities);

        // 获取房源下订单数据
        List<Order> notCancelOrders = orderMapper.notCancelOrders(houseId);

        List<Date> dates = new ArrayList<>();
        for(Order nco : notCancelOrders){
            // 获得入住时间和退房时间
            Date begin = nco.getArriveDate();
            Date end = nco.getLeaveDate();
            Long beginTime = begin.getTime();
            Long endTime  = end.getTime();
            Long oneDay = 1000 * 60 * 60 * 24l;

            while(beginTime < endTime){
                Date d = new Date(beginTime);
                dates.add(d);
                beginTime += oneDay;
            }
        }
        map.put("Dates", dates);
        return new ResponseBean().success("成功获得数据",map);
    }

    @Override
    public ResponseBean getHouseIndexMessage(String selectCityName) {
        Integer userId = getUserId();
        if(userId == null){}

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();

        List<String> hotCitiesName = (List<String>) valueOperations.get("hotCitiesName");
        if(hotCitiesName == null) {
            getHotCities();
            hotCitiesName = (List<String>) valueOperations.get("hotCitiesName");
        }
        List<Map<String,List<House>>> result =  (List<Map<String,List<House>>>) valueOperations.get("hotCities");


        Map<String,Object> map = new HashMap<>();
        map.put("hotCitiesName",hotCitiesName);
        String selectCity = null;
        if(selectCityName == null)
            selectCity = hotCitiesName.get(0);
        else
            selectCity = selectCityName;
        List<House> citiesOrder = new ArrayList<>();

        a : for(Map<String,List<House>> maps : result) {
            for (Map.Entry<String, List<House>> entry : maps.entrySet()) {
                String mapKey = entry.getKey();
                List<House> mapValue = entry.getValue();
                if (mapKey.equals(selectCity)) {
                    citiesOrder = mapValue;
                    break a;
                }
            }
        }
        map.put("citiesOrder",citiesOrder);

        return new ResponseBean().success("shuju",map);
    }

    @Override
    public ResponseBean selectCity(String cityName) {
        return null;
    }


    public List<Map<String,List<House>>> getHotCities(){

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        List<Map<String,List<House>>> result =  (List<Map<String,List<House>>>) valueOperations.get("hotCities");
        List<String> hotCitiesName = new ArrayList<>();
        // 没有的话从数据库中找
        if(result == null){

        List<Map<String,List<House>>> retrievalResult = new ArrayList<>();
        //获得所有订单
        List<Order> orders = orderMapper.selectList(null);

        for(Order order : orders){

            House house = fromRedisGetHouse(order.getHouseId());
            String[] url = house.getHouseImages().split(";");
            house.setAllDetailImages(Arrays.asList(url));
            //转一下图片格式
            //String allURL= order
            //房若间状态为停业，直接下一个订单
            if(house.getBusinessStatus() == 1) continue;
            HolidayHouse holidayHouse = fromRedisGetHolidayHouse(order.getHolidayHouseId());
            //获得经纬度
            String lng = holidayHouse.getLon();
            String lat = holidayHouse.getLat();
            //获得城市信息
            POIBean poiBean = LocationUtil.getPOIBean(lng, lat);
            if (poiBean == null) {
                continue;
            }
            String city = poiBean.getCity();

            if(retrievalResult.size() == 0){

                List<House> l = new ArrayList<>();
                Map<String,List<House>> map = new HashMap<>();
                l.add(house);
                map.put(city, l);
                retrievalResult.add(map);
                hotCitiesName.add(city);
                continue;
            }

            Boolean flagcity = false;

            for(String name : hotCitiesName){
                System.out.print(" " + name);
                if(name.equals(city)) {
                    flagcity = true;
                    break;
                }
            }

                //表示存在同名
                if(flagcity){
                    Map<String,List<House>> map = new HashMap<>();
                    for(int i=0; i < retrievalResult.size(); i++){
                        if(retrievalResult.get(i).containsKey(city))
                        map = retrievalResult.get(i);
                    }

                    List<House> l = map.get(city);
                    int i = 0;
                    for(House h : l){
                        i++;
                        if(h.getId() == house.getId()){
                            break;
                        }
                           // break a;
                        if (h.getId() != house.getId() && l.size() == i){
                            l.add(house);
                            System.out.println("true添加数据");
                           break;
                        }
                    }
                }
                else {
                    List<House> l = new ArrayList<>();
                    Map<String,List<House>> m = new HashMap<>();
                    l.add(house);
                    m.put(city, l);
                    retrievalResult.add(m);
                    hotCitiesName.add(city);
                }

        }
        System.out.println(retrievalResult);
            //存入redis，设定过期时间为48小时
            result = retrievalResult;
            valueOperations.set("hotCities", retrievalResult,30, TimeUnit.MINUTES);
            valueOperations.set("hotCitiesName", hotCitiesName,30, TimeUnit.MINUTES);
        }

        return result;
    }

    public static List<Date> findDates(Date dBegin, Date dEnd) {
        List lDate = new ArrayList();
        lDate.add(dBegin);
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime())) {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            Date d = calBegin.getTime();
            lDate.add(d);
        }
        return lDate;
    }

        // 获得商家id
    public Integer getUserId() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null) return null;
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }

       // if(currentUserName == null) return null;

        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        Integer merchantId = user.getId();
        return merchantId;
    }

    House fromRedisGetHouse (Integer houseId){
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        House house =  (House) valueOperations.get("house-" + houseId);
        // 没有的话从数据库中找
        if(house == null){
            house = houseMapper.selectOne(new QueryWrapper<House>().eq("id", houseId));
            //存入redis，设定过期时间为48小时
            valueOperations.set("house-" + houseId, house,48, TimeUnit.HOURS);
        }
        return house;
    }

    HolidayHouse fromRedisGetHolidayHouse (Integer holidayHouseId){
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        HolidayHouse holidayHouse =  (HolidayHouse) valueOperations.get("HolidayHouse-" + holidayHouseId);
        // 没有的话从数据库中找
        if(holidayHouse == null){
            holidayHouse = holidayHouseMapper.selectOne(new QueryWrapper<HolidayHouse>().eq("id", holidayHouseId));
            //存入redis，设定过期时间为48小时
            valueOperations.set("HolidayHouse-" + holidayHouseId, holidayHouse,48, TimeUnit.HOURS);
        }
        return holidayHouse;
    }
}
