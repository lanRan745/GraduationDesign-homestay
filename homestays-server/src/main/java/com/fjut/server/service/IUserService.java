package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.User;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-05
 */
public interface IUserService extends IService<User> {

    /**
     * 登录之后返回token
     *
     * @param username
     * @param password
     * @param role
     * @param response
     * @return
     */
    ResponseBean login(String username, String password, String validationCode, String role, HttpServletRequest request, HttpServletResponse response);

    User getUserByUsername(String username);

    //通过邮箱和角色信息获取用户个人信息
    User getUserByEmail(String email, String role);

    List<User> getAllUserInfo();

    ResponseBean register(User user);

    ResponseBean modifyPassword(String role, String password, String email);

    ResponseBean getSingleUserInfo();

    ResponseBean getSimpleUserInfo();

    ResponseBean modifyInfo(String realname, String idNumber,String telephone);

    ResponseBean updateAvatar(MultipartFile file) throws IOException;

    ResponseBean userModifyPassword(Integer userId, String newp, String oldp);

    ResponseBean powerSearch(String username, String role, Integer currentPage, Integer size);

    ResponseBean ban(Integer id);

    ResponseBean recovery(Integer id);
}
