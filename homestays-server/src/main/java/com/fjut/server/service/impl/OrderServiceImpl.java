package com.fjut.server.service.impl;

import com.alipay.api.AlipayApiException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.*;
import com.fjut.server.mapper.AliPayBeanMapper;
import com.fjut.server.mapper.CancelOrderMapper;
import com.fjut.server.mapper.OrderMapper;
import com.fjut.server.mapper.UserMapper;
import com.fjut.server.service.IOrderService;
import com.fjut.util.AliPayUtil;
import com.fjut.util.SystemTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-28
 */
@Service
@Primary
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private AliPayUtil aliPayUtil;
    @Autowired
    private AliPayBeanMapper aliPayBeanMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CancelOrderMapper cancelOrderMapper;

    @Override
    public ResponseBean notCancelOrders(Integer houseId) {
        List<Order>notCancelOrders = orderMapper.notCancelOrders(houseId);
        return new ResponseBean().success("获取未取消的订单",notCancelOrders);
    }

    /**
     * 创建新订单 订单过期时间修改为 40秒
     * @return
     */
    @Transactional
    public Integer createNewOrder(Order order) {
        int result = orderMapper.insert(order);
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        // 设置过期时间为1分钟
        valueOperations.set("pendingOrder:" + order.getId(), order.getId(),30, TimeUnit.SECONDS);
        return result;
    }

    @Override
    public ResponseBean payOrderAgain(AliPayBean aliPayBean,Integer id) {
        String time = redisTemplate.opsForValue().getOperations().getExpire("pendingOrder:"+id, TimeUnit.MINUTES ).toString();
        System.out.println("time================="+time);
        aliPayBean.setTimeout_express(time+"m");
        String pay = null;
        try {
            pay = aliPayUtil.pay(aliPayBean);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return new ResponseBean().success(pay);
    }

    private Long ONEDAY = 1000 * 60 * 60 * 24L;
    /**
     * 用户取消订单
     * @param map
     * @param id
     * @return
     */
    @Transactional
    @Override
    public ResponseBean userCancelOfOrder(Map<String, String> map, Integer id) {
        String out_trade_no = map.get("out_trade_no");
        String total_amount = map.get("total_amount");

        Order order = orderMapper.selectById(id);
        //判断状态是否在 2、3
        if(order.getOrderStatus() == 2 || order.getOrderStatus() == 3){
            //修改orderStatus状态
            order.setOrderStatus(-2);
            //确定isCancelStatus状态
            //获得系统当前时间
            Long now = System.currentTimeMillis();

            java.util.Date arriveDate = order.getArriveDate();
            java.util.Date arriveTime = order.getArriveTime();
            String tmp = arriveDate.toString() + " " + arriveTime.toString();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            java.util.Date adt = null;
            try {
                adt = simpleDateFormat.parse(tmp);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            // 订单开始的时间戳
            long arriveDateTime = adt.getTime();

            // 大于一天之内取消订单
            if(arriveDateTime - now > ONEDAY){
                if(aliPayUtil.refund(total_amount, out_trade_no)){
                    // 设置取消状态 1:表示全额退款
                    order.setIsCancelStatus(1);
                    // 修改orderStatus -> -2
                    order.setOrderStatus(-2);
                    CancelOrder cancelOrder = new CancelOrder();
                    //String cancelDate = simpleDateFormat.format(new java.util.Date());
                    //插入取消订单表
                    cancelOrder.setCancelDateTime(SystemTimeUtil.getCurrentTime());
                    cancelOrder.setRefundPrice(order.getPrice());
                    cancelOrderMapper.insert(cancelOrder);
                    //获得取消订单字段id
                    order.setCancelOrderId(cancelOrder.getId());
                    //设置备注信息
                    order.setRemarks("您的订单在入住24小时前取消，Arib为您全额退款");
                    orderMapper.updateById(order);
                    return new ResponseBean().success("您的订单在入住24小时前取消，Arib为您全额退款！");
                }else {
                    return new ResponseBean().error("退款失败，请重试！");
                }
            }
            // 取消时间距离入住时间在24小时以内，该收费啦
            else if( arriveDateTime - now > 0 && arriveDateTime - now <= ONEDAY){
                DecimalFormat df = new DecimalFormat("#.00");
                // 计算退款金额
                Integer day_amount = order.getDayAmount();
                Double total_price = order.getPrice();
                // 收取首晚房费
                Double firstDay_profit = Double.valueOf(df.format(total_price / day_amount));
                // 剩余需要退款的房费
                Double surplus = total_price - firstDay_profit;

                // 平台获利
                Double airb_profit = Double.valueOf(df.format(firstDay_profit * 0.1));
                // 商家获利
                Double merchant_profit = Double.valueOf(df.format(firstDay_profit * 0.9));

                if(aliPayUtil.refund(surplus.toString(), out_trade_no) || surplus == 0){
                    // 设置取消状态 2:表示收取首晚房费
                    order.setIsCancelStatus(2);
                    // 修改orderStatus -> -2
                    order.setOrderStatus(-2);
                    CancelOrder cancelOrder = new CancelOrder();
                    //插入取消订单表
                    cancelOrder.setCancelDateTime(SystemTimeUtil.getCurrentTime());
                    cancelOrder.setRefundPrice(surplus);
                    cancelOrderMapper.insert(cancelOrder);
                    //获得取消订单字段id
                    order.setCancelOrderId(cancelOrder.getId());
                    //平台和商家获利
                    order.setMerchantGetPrice(merchant_profit);
                    order.setAirbGetPrice(airb_profit);
                    //设置备注信息
                    order.setRemarks("您的订单在入住24小时内取消，Arib按照取消政策收取首晚费用");
                    int result = orderMapper.updateById(order);
                    return result > 0 ? new ResponseBean().success("您的订单在入住24小时内取消，Arib按照取消政策收取首晚费用，具体信息请在【过往的旅程】中查看") : new ResponseBean().error("退款失败，请重试！");
                }
                else {
                    return new ResponseBean().error("退款失败，请重试！");
                }
            }
            // 表示用户已入住
            else if(arriveDateTime - now <= 0){
                // 24小时后
                Long oneDayAfter = now + ONEDAY;
                java.util.Date leaveDate = order.getLeaveDate();
                java.util.Date beginDate = order.getArriveDate();
                java.util.Date leaveTime = order.getLeaveTime();
                java.util.Date beginTime = order.getArriveTime();
                String leaveDateTime = leaveDate.toString() + " " + beginTime.toString();
                String beginDateTime = beginDate.toString() + " " + beginTime.toString();

                java.util.Date ldate = null, bdate = null;
                try {
                    ldate = simpleDateFormat.parse(leaveDateTime);
                    bdate = simpleDateFormat.parse(beginDateTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // 订单结束时间戳
                long leaveTimeStamp = ldate.getTime();
                // 订单开始时间戳
                long arriveTimeStamp = bdate.getTime();
                long temp = arriveTimeStamp;
                while(oneDayAfter > temp){
                    temp += ONEDAY;
                }
                Integer refund_day_amount = 0;
                while(temp < leaveTimeStamp){
                    refund_day_amount++;
                    temp += ONEDAY;
                }

                DecimalFormat df = new DecimalFormat("#.00");
                // 租赁天数
                Integer day_amount = order.getDayAmount();
                // 获得总价
                Double total_price = order.getPrice();
                Double already_check_profit = 0d;
                if(refund_day_amount == 0)
                    already_check_profit = Double.valueOf(df.format(total_price));
                //  剩余需要退款的房费
                else
                    already_check_profit = Double.valueOf(df.format(total_price * refund_day_amount / day_amount));
                // 收取入住天数房费
                Double surplus = total_price - already_check_profit;

                // 平台获利
                Double airb_profit = Double.valueOf(df.format(surplus * 0.1));
                // 商家获利
                Double merchant_profit = Double.valueOf(df.format(surplus * 0.9));

                if(aliPayUtil.refund(already_check_profit.toString(), out_trade_no) || already_check_profit == 0){
                    System.out.println("----------------5----------------------");
                    // 设置取消状态 3:表示已入住后收费
                    order.setIsCancelStatus(3);
                    // 修改orderStatus -> -2
                    order.setOrderStatus(-2);
                    CancelOrder cancelOrder = new CancelOrder();
                    //插入取消订单表
                    cancelOrder.setCancelDateTime(SystemTimeUtil.getCurrentTime());
                    cancelOrder.setRefundPrice(already_check_profit);
                    cancelOrder.setRefundDayAmount(refund_day_amount);
                    cancelOrderMapper.insert(cancelOrder);

                    //获得取消订单字段id
                    order.setCancelOrderId(cancelOrder.getId());
                    //平台和商家获利
                    order.setMerchantGetPrice(merchant_profit);
                    order.setAirbGetPrice(airb_profit);
                    //设置备注信息
                    order.setRemarks("您已入住，当前提前退房将取消预订24小时后的未住宿晚数的房费全额退还。");
                    int result = orderMapper.updateById(order);
                    return result > 0
                            ?
                            new ResponseBean().success("您已入住，当前提前退房将取消预订24小时后的未住宿晚数的房费全额退还，具体信息请在【过往的旅程】中查看")
                            :
                            new ResponseBean().error("退款失败，请重试！");
                }
                else {
                    return new ResponseBean().error("退款失败，请重试！");
                }
            }
        }
        return new ResponseBean().success("测试退款接口");
    }

    @Override
    public ResponseBean getCheckOrders(Integer currentPage, Integer size) {
        Integer merchantId = getUserId();
        Page<Order> page = new Page<>(currentPage,size);
        IPage<Order> IPage =  orderMapper.getCheckOrders(page,merchantId);
        Map<String,Object> map = new HashMap<>();
        map.put("total",IPage.getTotal());
        map.put("records",IPage.getRecords());
        return new ResponseBean().success("获取订单信息",map);
    }

    /**
     * 确定订单
     * @param id
     * @return
     */
    @Override
    public ResponseBean determineOrder(@RequestParam("id") Integer id) {
        Order order = new Order();
        order.setId(id);
        order.setOrderStatus(3);
        int result = orderMapper.updateById(order);
        return result > 0 ? new ResponseBean().success("成功确定订单") : new ResponseBean().error("订单确定失败，请重试！");
    }

    @Override
    public ResponseBean searchCheckOrders(Integer currentPage, Integer size, String username, String email, Integer orderStatus,Integer checkStatus, String no) {
        Integer merchantId = getUserId();
        Page<Order> page = new Page<>(currentPage,size);
        IPage<Order> IPage =  orderMapper.searchCheckOrders(page,merchantId,username,email,orderStatus,checkStatus,no);
        Map<String,Object> map = new HashMap<>();
        map.put("total",IPage.getTotal());
        map.put("records",IPage.getRecords());
        return new ResponseBean().success("获取订单信息",map);
    }

    @Override
    public ResponseBean getMerchantAllOrders(Integer currentPage, Integer size, String username, String email, Integer orderStatus, String no) {
        Integer merchantId = getUserId();
        Page<Order> page = new Page<>(currentPage,size);
        IPage<Order> IPage =  orderMapper.getMerchantAllOrders(page,merchantId,username,email,orderStatus,no);
        Map<String,Object> map = new HashMap<>();
        map.put("total",IPage.getTotal());
        map.put("records",IPage.getRecords());
        return new ResponseBean().success("获取订单信息",map);
    }


    @Override
    public ResponseBean getOrderStatistics() {
        Integer merchantId = getUserId();

        //计算第一个月的数据
        List<Order> oneMonthOrders = orderMapper.getOneMonthOrders(merchantId);
        List<Map<String,Object>> list = new ArrayList<>();
        Map<String,Object> four = new HashMap<>();
        four.put("value", 0);
        four.put("name", "订单已完成");
        Map<String,Object> zero = new HashMap<>();
        zero.put("value", 0);
        zero.put("name", "订单关闭");
        Map<String,Object> one = new HashMap<>();
        one.put("value", 0);
        one.put("name", "房东未确认");
        Map<String,Object> two = new HashMap<>();
        two.put("value", 0);
        two.put("name", "用户取消订单");
        Map<String,Object> three = new HashMap<>();
        three.put("value", 0);
        three.put("name", "房东取消订单");
        Map<String,Object> other = new HashMap<>();
        other.put("value", 0);
        other.put("name", "正在进行的订单");
        System.out.println(oneMonthOrders.size());
        for(Order o : oneMonthOrders){
            Integer s = o.getOrderStatus();
            if(s == 4){
                Integer v = (Integer) four.get("value");
                four.put("value",++v);
            }
            if(s == 0){
                Integer v = (Integer) zero.get("value");
                zero.put("value",++v);
            }
            else if(s == -1){
                Integer v = (Integer) one.get("value");
                one.put("value",++v);
            }
            else if(s == -2){
                Integer v = (Integer) two.get("value");
                two.put("value",++v);
            }
            else if(s == -3){
                Integer v = (Integer) three.get("value");
                three.put("value",++v);
            }
            else {
                Integer v = (Integer) other.get("value");
                other.put("value",++v);
            }
        }
        list.add(four);
        list.add(zero);
        list.add(one);
        list.add(two);
        list.add(three);
        list.add(other);

        List<Map<String,Object>> l = new ArrayList<>();
        //List<String> names = new ArrayList<>();

        for(Order o : oneMonthOrders){
            String address = o.getHolidayHouse().getAddress();
            //for()
            if(l.size() == 0){
                Map<String,Object> m = new HashMap<>();
                //names.add(address);
                m.put("name", address);
                m.put("value",1);
                l.add(m);
            }
            else {
                boolean flag = false;
                for(Map map : l){
                    if(map.containsValue(address)){
                        flag = true;
                        map.put("value", (Integer)map.get("value") + 1);
                        break;
                    }
                }
                if(!flag){
                    Map<String,Object> m = new HashMap<>();
                    m.put("name", address);
                    m.put("value",1);
                    l.add(m);
                }
                flag = false;
            }
        }
        System.out.println(list);
        System.out.println(l);
        Map<String,Object> result = new HashMap<>();
        result.put("f", list);
        result.put("s", l);

        return new ResponseBean().success("11",result);
    }




    @Override
    public ResponseBean merchantCancelOrder(Integer orderId, String remarks, String out_trade_no, Integer dayAmount) {
        Order order = orderMapper.selectById(orderId);
        if(order.getOrderStatus() != 2)
            return new ResponseBean().error("订单取消失败，请刷新重试");

        if(aliPayUtil.refund(order.getPrice().toString(), out_trade_no)){
            //修改订单状态 -3表示商家退款
            order.setOrderStatus(-3);
            //添加备注
            order.setRemarks(remarks);
            //退款申请（全额退款） 设置取消状态 1:表示全额退款
            order.setIsCancelStatus(1);
            CancelOrder cancelOrder = new CancelOrder();
            //插入取消订单表
            cancelOrder.setCancelDateTime(SystemTimeUtil.getCurrentTime());
            cancelOrder.setRefundPrice(order.getPrice());
            cancelOrder.setRefundDayAmount(dayAmount);
            cancelOrderMapper.insert(cancelOrder);
            //获得取消订单字段id
            order.setCancelOrderId(cancelOrder.getId());
            orderMapper.updateById(order);
            return new ResponseBean().success("取消订单成功");
        }
        return new ResponseBean().error("退款失败，请重试！");
    }


    @Transactional
    @Override
    public ResponseBean userOrderPay(Map<String,String> map,String total_amount) {
        AliPayBean aliPayBean = new AliPayBean();

        Order order = new Order();
        order.setMerchantId(Integer.valueOf(map.get("merchantId")));
        order.setHolidayHouseId(Integer.valueOf(map.get("holidayHouseId")));
        order.setHouseId(Integer.valueOf(map.get("houseId")));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        try {
            java.util.Date ad = format.parse(map.get("arriveDate"));
            java.util.Date ld = format.parse(map.get("leaveDate"));
            order.setArriveDate(new java.sql.Date(ad.getTime()));
            order.setLeaveDate(new java.sql.Date(ld.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String arriveTime =  map.get("arriveTime");
        String leaveTime =  map.get("leaveTime");

        try {
            SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
            java.util.Date d1 = f.parse(arriveTime);
            java.util.Date d2 = f.parse(leaveTime);
            Time t1 = new Time(d1.getTime()).valueOf(arriveTime);
            Time t2 = new Time(d2.getTime()).valueOf(leaveTime);
            order.setArriveTime(t1);
            order.setLeaveTime(t2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        order.setPrice(Double.valueOf(map.get("price")));
        order.setDayAmount(Integer.valueOf(map.get("dayAmount")));
        Integer userId = getUserId();
        order.setUserId(userId);

        long time = System.currentTimeMillis();
        Timestamp timestamp = new Timestamp(time);
        order.setCreateTime(timestamp);

        //付款金额
        aliPayBean.setTotal_amount(total_amount);
        aliPayBean.setOut_trade_no(getRandomString(20));//随机字符
        System.out.println(aliPayBean);
        String pay = null;
        try {
            pay = aliPayUtil.pay(aliPayBean);
            //将aliPayBean存入数据库
            int result = aliPayBeanMapper.insert(aliPayBean);
            if(result > 0) {
                System.out.println("====================aliPayBean================");
                System.out.println(aliPayBean);
                order.setAliPayBeanId(aliPayBean.getId());
                int r = createNewOrder(order);
                if(r > 0)
                    return new ResponseBean().success(pay);
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return new ResponseBean().error("发生错误，请重试");
    }


    @Override
    public ResponseBean getPastUserOrders(Integer currentPage, Integer size) {
        Integer userId = getUserId();
        Page<Order> page = new Page<>(currentPage,size);
        IPage<Order> Ipage =  orderMapper.getPastUserOrders(page,userId);
        Map<String,Object> map = new HashMap<>();
        List<Order> order = Ipage.getRecords();
        for(Order o : order) {
            String[] url = o.getHouse().getHouseImages().split(";");
            o.getHouse().setHouseImages(url[0]);
            System.out.println(url[0]);
        }
        map.put("pastOrdersTotal",Ipage.getTotal());
        map.put("pastOrdersRecords",Ipage.getRecords());
        return new ResponseBean().success("获取过往订单",map);
    }


    @Override
    public ResponseBean getAheadUserOrders(Integer currentPage, Integer size) {
        Integer userId = getUserId();
        Page<Order> page = new Page<>(currentPage,size);
        IPage<Order> Ipage =  orderMapper.getAheadUserOrders(page,userId);
        Map<String,Object> map = new HashMap<>();
        List<Order> order = Ipage.getRecords();
        for(Order o : order) {
            String[] url = o.getHouse().getHouseImages().split(";");
            o.getHouse().setHouseImages(url[0]);
        }
        map.put("aheadOrdersTotal",Ipage.getTotal());
        map.put("aheadOrdersRecords",Ipage.getRecords());
        return new ResponseBean().success("获取即将开始的订单",map);
    }


    public String getRandomString(int length) {
        String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(str.charAt(number));
        }
        return sb.toString();
    }


    public Integer getUserId() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null) return null;
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }

        // if(currentUserName == null) return null;

        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        Integer merchantId = user.getId();
        return merchantId;
    }

}
