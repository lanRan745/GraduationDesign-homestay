package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Facility;
import com.fjut.server.mapper.FacilityMapper;
import com.fjut.server.service.IFacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-27
 */
@Service
@Primary
public class FacilityServiceImpl extends ServiceImpl<FacilityMapper, Facility> implements IFacilityService {

    @Autowired
    private FacilityMapper facilityMapper;

    /**
     * 得到所有的便利设施
     * @return
     */
    @Override
    public ResponseBean getFacilities() {

        List<Facility> list = facilityMapper.selectList(null);

        return new ResponseBean().success("成功获取所有便利设施",list);
    }
}
