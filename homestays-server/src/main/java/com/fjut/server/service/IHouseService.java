package com.fjut.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.House;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
public interface IHouseService extends IService<House> {

    ResponseBean addNewHouse(Map<String,Object> map, MultipartFile[] files) throws IOException;

    ResponseBean getHouses(Integer holidayHouseId);

    ResponseBean deleteHouse(Integer houseId);

    ResponseBean modifyBusinessStatus(Integer houseId, Integer businessStatus);

    ResponseBean getHouseDetailMessage(Integer houseId);

    ResponseBean getHouseIndexMessage(String selectCityName);

    ResponseBean selectCity(String cityName);
}
