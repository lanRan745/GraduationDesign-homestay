package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.server.entity.CancelOrder;
import com.fjut.server.mapper.CancelOrderMapper;
import com.fjut.server.service.ICancelOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-28
 */
@Service
public class CancelOrderServiceImpl extends ServiceImpl<CancelOrderMapper, CancelOrder> implements ICancelOrderService {

}
