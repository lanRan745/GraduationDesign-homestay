package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Comment;
import com.fjut.server.entity.Order;
import com.fjut.server.entity.User;
import com.fjut.server.mapper.CommentMapper;
import com.fjut.server.mapper.OrderMapper;
import com.fjut.server.mapper.UserMapper;
import com.fjut.server.service.ICommentService;
import com.fjut.util.BadWordsUtil;
import com.fjut.util.SystemTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
@Service
@Primary
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements ICommentService {

    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    // 获得商家
    public  User getCurrentUser() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        return user;
    }

    @Override
    public ResponseBean getCommentAmount() {
        User u = getCurrentUser();
        int amount = commentMapper.selectCount(new QueryWrapper<Comment>().eq("userId", u.getId()));
        return new ResponseBean().success("获取评论条数",amount);
    }

    /**
     *
     * @param currentPage
     * @param size
     * @param houseId
     * @return
     */
    @Override
    public ResponseBean getComments(Integer currentPage, Integer size, Integer houseId) {
        Page<Comment> page = new Page<>(currentPage,size);
        IPage<Comment> commentIpage =  commentMapper.getCommentsPage(page,houseId);
        Map<String,Object> map = new HashMap<>();

        map.put("CommentTotal",commentIpage.getTotal());
        map.put("CommentRecords",commentIpage.getRecords());

        Double commentValue = commentMapper.getRating(houseId);
        map.put("commentValue",commentValue);

        return new ResponseBean().success("获取评论分页",map);
    }

    @Transactional
    @Override
    public ResponseBean addComment(Integer rating, String content, Integer orderId, Integer userId, Integer houseId) {

        // 敏感词检测
        try {
            if (BadWordsUtil.isContaintSensitiveWord(BadWordsUtil.initBadWordList(),content)) {
                return new ResponseBean().error("检测出了敏感词！请重新输入");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Comment c = new Comment();
        c.setUserId(userId);
        c.setHouseId(houseId);
        c.setOrderId(orderId);
        c.setCreateTime(SystemTimeUtil.getCurrentTime());
        c.setContent(content);
        c.setRating(rating);
        commentMapper.insert(c);
        Order order = new Order();
        order.setId(orderId);
        order.setCommentId(c.getId());
        int result = orderMapper.updateById(order);

        return result > 0 ? new ResponseBean().success("评论成功") : new ResponseBean().error("评论失败");
    }
}
