package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.*;
import com.fjut.server.mapper.*;
import com.fjut.server.service.IHolidayHouseService;
import com.fjut.util.LocationUtil;
import com.fjut.util.OSSUtil;
import com.fjut.util.SystemTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-23
 */
@Service
@Primary
public class HolidayHouseServiceImpl extends ServiceImpl<HolidayHouseMapper, HolidayHouse> implements IHolidayHouseService  {

    @Autowired
    private OSSUtil ossUtil;
    @Autowired
    private HolidayHouseMapper holidayHouseMapper;
    @Autowired
    private HouseMapper houseMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private LogMapper logMapper;
    @Autowired
    private OrderMapper orderMapper;


    @Override
    public ResponseBean addHolidayHouse(String lon, String lat, String bdAddress, String address, String province, String city, String area, String describe, String detailDescribe, MultipartFile[] files) throws IOException {

        // 获得图片路径
        StringBuffer url = new StringBuffer();

        for(int i=0; i < files.length ;i++){
            if(i==0){
                url.append(ossUtil.uploadFile(files[i],"相关证件"));
            }else{
                url.append(";"+ossUtil.uploadFile(files[i],"相关证件"));
            }
        }
        Integer merchantId = getUserId();
        HolidayHouse holidayHouse = new HolidayHouse(); //7个字段，3个有默认值
        holidayHouse.setAddress(address);
        holidayHouse.setLat(lat);
        holidayHouse.setLon(lon);
        holidayHouse.setBdAddress(bdAddress);
        holidayHouse.setHolidayHouseDescribe(describe);
        holidayHouse.setImages(url.toString());
        holidayHouse.setMerchantId(merchantId);
        holidayHouse.setCreateTime(SystemTimeUtil.getCurrentTime());
        holidayHouse.setDetailDescribe(detailDescribe);

        //存入数据库
        int result = holidayHouseMapper.insert(holidayHouse);
        System.out.println("测试输出=======================================");
        System.out.println(holidayHouse.toString());

        //添加到日志中
        if(result > 0){
            Log log = new Log();
            long time = System.currentTimeMillis();
            Timestamp timestamp = new Timestamp(time);
            log.setCreateTime(timestamp);
            log.setSender("Arinb团队");
            log.setTitle("审核申请已发送");
            log.setUserId(merchantId);
            log.setMessage("您的新申请的民宿审核申请已发送，工作人员会在5个工作日内审核完毕，请耐心等待工作人员的审核！谢谢您的支持。");

            logMapper.insert(log);


            addHouseToRedis(holidayHouse);

            return new ResponseBean().success("添加成功，请等待工作人员审核通过");
        }
        return  new ResponseBean().error("添加失败，新重试！");
    }

    /**
     * 将房源经纬度转换为一维数据，并存入redis中
     */
    public void addHouseToRedis(HolidayHouse holidayHouse){
        String key = "HousePosition";
        String name = "" + holidayHouse.getId();
        // 要存入的地点的经纬度坐标值
        Point point = new Point(Double.parseDouble(holidayHouse.getLon()),Double.parseDouble(holidayHouse.getLat()));
        // 要存入到 GeoHash中的元素，由 name 和 经纬度坐标值组成
        RedisGeoCommands.GeoLocation<String> geoLocation = new RedisGeoCommands.GeoLocation<>(name,point); // name + score (这个是HashGeo中的值）
        // 存入 GeoHash
        redisTemplate.opsForGeo().add(key,geoLocation);
    }

    /**
     * 房东概览：获得名下所有民宿信息
     * @return
     */
    @Override
    public ResponseBean scanHomestays() {

        // 获得当前商家信息
        Integer merchantId = getUserId();
        List<HolidayHouse> list = holidayHouseMapper
                                    .selectList(new QueryWrapper<HolidayHouse>().eq("merchantId", merchantId));
        for (int i = 0; i < list.size(); i++) {
            HolidayHouse holidayHouses = list.get(i);
            String AllUrl = holidayHouses.getImages();
            String Urls[] = AllUrl.split(";");
            List<String> l = new ArrayList();
            for (String url :  Urls) {
                l.add(url);
            }
            holidayHouses.setAllImages(l);
        }

        return new ResponseBean().success("查到啦",list);
    }


    /**
     *
     * @param id
     * @param auditStatus 管理员审核状态
     * @return
     */
    @Override
    public ResponseBean modifyAuditStatus(Integer id, Integer auditStatus) {

         HolidayHouse holidayhouse = new HolidayHouse();
         holidayhouse.setId(id);
         holidayhouse.setAuditStatus(auditStatus);

        //判断审核状态
        //审核通过
         if(auditStatus == 2){
             if (holidayHouseMapper.updateById(holidayhouse) == 0) {
                 return new ResponseBean().error("修改审核状态失败，请重试");
             }
             //修改成功，发送系统消息
             HolidayHouse hh = holidayHouseMapper.selectOne(new QueryWrapper<HolidayHouse>().eq("id", id));

             Log log = new Log();
             log.setTitle("位于"+"申请审核通过");
             log.setSender("Arinb团队");
             long time = System.currentTimeMillis();
             Timestamp timestamp = new Timestamp(time);
             log.setCreateTime(timestamp);
             log.setMessage("恭喜您，您的申请已经通过！接下来，您需要将向申请的民宿中添加具体的房源信息，达到要求后就可以将房屋上线啦！");
         }

         //审核不通过
        return new ResponseBean().success("qq");

    }

    /**
     * 修改房源信息
     * @param describe
     * @param detailDescribe
     * @param id
     * @param auditStatus
     * @param files
     * @return
     */
    @Override
    public ResponseBean modifyHomestays(String describe, String detailDescribe, String id, Integer auditStatus, MultipartFile[] files) throws IOException{

        Integer merchantId = getUserId();
        HolidayHouse holidayHouse = new HolidayHouse(); //7个字段，3个有默认值
        holidayHouse.setHolidayHouseDescribe(describe);
        holidayHouse.setAuditStatus(auditStatus);
        holidayHouse.setDetailDescribe(detailDescribe);

        if(files != null) {

            // 获得图片路径
            StringBuffer url = new StringBuffer();

            for (int i = 0; i < files.length; i++) {
                if (i == 0) {
                    url.append(ossUtil.uploadFile(files[i], "相关证件"));
                } else {
                    url.append(";" + ossUtil.uploadFile(files[i], "相关证件"));
                }
            }
            holidayHouse.setImages(url.toString());
        }

        //存入数据库
        int result = holidayHouseMapper.update(holidayHouse,new UpdateWrapper<HolidayHouse>().eq("merchantId", merchantId).eq("id", id));

        //添加到日志中
        if (result > 0) {
            Log log = new Log();
            long time = System.currentTimeMillis();
            Timestamp timestamp = new Timestamp(time);
            log.setCreateTime(timestamp);
            log.setSender("Arinb团队");
            log.setTitle("房源信息已修改");
            log.setUserId(merchantId);
            log.setMessage("您的源信息已作出修改！");

            logMapper.insert(log);
            return new ResponseBean().success("修改成功");
        }
        return new ResponseBean().error("修改失败");
    }

    @Override
    public ResponseBean getLongAndLati(String lng, String lat, Integer buttonStatus, Integer min, Integer max, Integer iscancel) {
        Point point = new Point(Double.parseDouble(lng),Double.parseDouble(lat));
        List<House> houses = (List<House>)getNearbyHouses(point).get("nearbyHouses");
        List<House> filterHouses = new ArrayList<>();
        //查找可免费取消的民宿
        if(iscancel == 1){
            for(House h : houses){
                if(h.getIsCancel() == 1){
                    filterHouses.add(h);
                }
            }
        }else{
            filterHouses = houses;
        }
        List<House> fh = new ArrayList<>();
        if(buttonStatus == 1){
            for(House h : filterHouses){
                Double p = h.getPrice()*h.getDiscount()/10;
                if(p> min && p<= max){
                    fh.add(h);
                }
            }
        }else{
            fh = filterHouses;
        }
        System.out.println("fh===>"+fh.size());
        return new ResponseBean().success("获取附近房源！！",fh);
    }

    @Override
    public ResponseBean verifyHolidayHouses(Integer currentPage, Integer size) {

        Page<HolidayHouse> page = new Page<>(currentPage,size);
        IPage Ipage = holidayHouseMapper.verifyHolidayHouses(page);

        List<HolidayHouse> holidayHouses = Ipage.getRecords();

        for(HolidayHouse h : holidayHouses){
            String[] url = h.getImages().split(";");
            h.setAllImages(Arrays.asList(url));
        }

        Map<String,Object> map = new HashMap<>();
        map.put("total",Ipage.getTotal());
        map.put("records",Ipage.getRecords());
        return new ResponseBean().success("待审核订单",map);
    }

    @Override
    public ResponseBean rejectHolidayHouse(Integer id, Integer merchantId, String address, String remark) {
        HolidayHouse holidayHouse = new HolidayHouse();
        holidayHouse.setId(id);
        holidayHouse.setAuditStatus(0);
        Log log = new Log();
        //添加审核失败信息
        if(holidayHouseMapper.updateById(holidayHouse) > 0){

            log.setTitle("房源申请【驳回】通知");
            log.setSender("Arinb团队");
            log.setUserId(merchantId);
            long time = System.currentTimeMillis();
            Timestamp timestamp = new Timestamp(time);
            log.setCreateTime(timestamp);
            log.setMessage("您的申请位于【"+address+"】的房源申请失败！"+remark);
        }
        return  logMapper.insert(log)> 0 ? new ResponseBean().success("审核驳回成功") : new ResponseBean().error("审核驳回失败");
    }

    @Transactional
    @Override
    public ResponseBean passHolidayHouse(Integer id, Integer merchantId, String address) {
        HolidayHouse holidayHouse = new HolidayHouse();
        holidayHouse.setId(id);
        holidayHouse.setAuditStatus(2);
        Log log = new Log();
        //添加审核失败信息
        if(holidayHouseMapper.updateById(holidayHouse) > 0){

            log.setTitle("房源申请通过通知");
            log.setSender("Arinb团队");
            log.setUserId(merchantId);
            long time = System.currentTimeMillis();
            Timestamp timestamp = new Timestamp(time);
            log.setCreateTime(timestamp);
            log.setMessage("恭喜您，您的申请位于【"+address+"】的房源已经通过！接下来，您需要将向申请的民宿中添加具体的房间信息，达到要求后就可以将房屋上线啦！");
        }
        return  logMapper.insert(log)> 0 ? new ResponseBean().success("审核成功通过") : new ResponseBean().error("审核通过失败");
    }

    @Override
    public ResponseBean getAdminIndexData() {

        Map<String,Object> map = new HashMap<>();
        //需要审核的房源数量
        map.put("amount",getNeedVerifyHolidayHouse());
        map.put("province",calculateHolidayHousePosition());
        map.put("orderCreateTime",calculateOrderCreateTime());


        return new ResponseBean().success("成功获取数据",map);
    }

    public Integer getNeedVerifyHolidayHouse(){
        return holidayHouseMapper.selectCount(new QueryWrapper<HolidayHouse>().eq("auditStatus", "1"));
    }

    public List<Map<String,Object>> calculateHolidayHousePosition(){
        List<HolidayHouse> holidayHouses = holidayHouseMapper.selectList(null);

        List<Map<String,Object>> l = new ArrayList<>();

        Map<String,Object> result= new HashMap<>();
        for(HolidayHouse hh : holidayHouses){
            String province = LocationUtil.getPOIBean(hh.getLon(),hh.getLat()).getProvince();

            if(l.size() == 0){
                Map<String,Object> m = new HashMap<>();
                m.put("name", province);
                m.put("value",1);
                l.add(m);
            }
            else {
                boolean flag = false;
                System.out.print(l);
                for(Map map : l){
                    if(map.get("name").equals(province)){
                        System.out.println("房源分布图 加1");
                        flag = true;
                        map.put("value", (Integer)map.get("value") + 1);
                        break;
                    }
                }
                if(!flag){
                    Map<String,Object> m = new HashMap<>();
                    m.put("name", province);
                    m.put("value",1);
                    l.add(m);
                }
                flag = false;
            }

        }
        return l;
    }

    public Object[] calculateOrderCreateTime(){
        List<Order> orders = orderMapper.selectList(null);
        List<String> x = new ArrayList<>();
        List<Integer> y = new ArrayList<>();

        a : for(Order o : orders){

            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
            Timestamp t = o.getCreateTime();
            String time = sdf.format(t);

            if(x.size() == 0){
                x.add(time);
                y.add(1);
            }
            else {
                //boolean flag = false;
                for(int i = 0; i < x.size(); i++ ){
                    if(x.get(i).equals(time)){
                        y.set(i, y.get(i) + 1);
                        continue a;
                    }
                }
                x.add(time);
                y.add(1);
            }
        }
        Object[] o = new Object[2];
        o[0] = x;
        o[1] = y;
        return o;
    }



    /**
     * 用户选择地址查询附近民宿
     * @param point 传进来的经纬度point
     * @return
     */
    public Map getNearbyHouses(Point point) {
        String key = "HousePosition";
        Map<String,Object> map = new HashMap<>();
        List<HolidayHouse> hh = new ArrayList<>();
        List<Object> names = new ArrayList<>();
        List<Double> distances = new ArrayList<>();

        // 首先圈定一个圆形范围
        Distance distance = new Distance(5, Metrics.KILOMETERS); // 圆形半径长度
        Circle circle = new Circle(point, distance); // 圆心的位置

        // GeoResults 中存放了 检索的结果
        // radius(key,circle) ===> 检索GeoHash中在圆形中的所有地点
        GeoResults radius = redisTemplate.opsForGeo().radius(key, circle);

        // List<GeoResult> 存放的就是 检索到的地点（也就是GeoHash中的元素）的集合
        List<GeoResult> content = radius.getContent();

        for(GeoResult c : content){

            // 获取到元素的内容
            /*
            RedisGeoCommands.GeoLocation<T>
            有两个对象 T name 和 Point point
            分别为 name  和 经纬度坐标
            我们就可以通过这个RedisGeoCommands.GeoLocation 对象 来获取到 我们需要的 name
            或者 经纬度坐标  他还有其他属性，可以自己断点调试看一下
            */
            RedisGeoCommands.GeoLocation<String> s = (RedisGeoCommands.GeoLocation) c.getContent();
            String member = s.getName(); // 获取到 name
            HolidayHouse h = holidayHouseMapper.selectById(Integer.parseInt(member));
            if(h != null) {
                hh.add(h);
                names.add(member);
            }
        }
        List<Point> addressPoint = redisTemplate.opsForGeo().position("HousePosition",names);

        map.put("nearbyHouses",getYYHouse(hh));
        map.put("Point",addressPoint);
        return map;
    }

    public List<House> getYYHouse(List<HolidayHouse> holidayHouse){
        List<House> houses = new ArrayList<>(); // 所有房间集合
        for(HolidayHouse h : holidayHouse){
            List<House> house = houseMapper.selectList(new QueryWrapper<House>().eq("holidayHouseId", h.getId()).eq("businessStatus", 2));
            for(House ho : house) {
                HolidayHouse HH = holidayHouseMapper.selectById(ho.getHolidayHouseId());
                ho.setHh(HH);
                houses.add(ho);
            }
        }
        //处理图片
        for (int i = 0; i < houses.size(); i++) {
            House hou = houses.get(i);
            String AllUrl = hou.getHouseImages();
            String Urls[] = AllUrl.split(";");
            List<String> l = new ArrayList();
            for (String url :  Urls) {
                l.add(url);
            }
            hou.setAllDetailImages(l);
        }
        return houses;
    }


    // 获得商家id
    public Integer getUserId() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }

        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        Integer merchantId = user.getId();
        return merchantId;
    }
}
