package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.server.entity.AliPayBean;
import com.fjut.server.mapper.AliPayBeanMapper;
import com.fjut.server.service.IAliPayBeanService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * @Author lanRan
 * @Date 2021/5/26 15:22
 * @Version 1.0
 **/

@Service
@Primary
public class AliPayBeanServiceImpl extends ServiceImpl<AliPayBeanMapper, AliPayBean> implements IAliPayBeanService {
}
