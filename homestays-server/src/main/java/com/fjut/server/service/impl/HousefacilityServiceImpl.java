package com.fjut.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjut.server.entity.Housefacility;
import com.fjut.server.mapper.HousefacilityMapper;
import com.fjut.server.service.IHousefacilityService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-27
 */
@Service
public class HousefacilityServiceImpl extends ServiceImpl<HousefacilityMapper, Housefacility> implements IHousefacilityService {

}
