package com.fjut.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_order")
@ApiModel(value="Order对象", description="")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "房东id")
    private Integer merchantId;

    @ApiModelProperty(value = "房源id")
    private Integer holidayHouseId;

    @ApiModelProperty(value = "房间id")
    private Integer houseId;

    @ApiModelProperty(value = "支付id")
    private Integer aliPayBeanId;

    @ApiModelProperty(value = "评论id")
    private Integer commentId;

    @ApiModelProperty(value = "取消订单id")
    private Integer cancelOrderId;

    @ApiModelProperty(value = "订单生成时间")
    private Timestamp createTime;

    @ApiModelProperty(value = "规定入住日期")
    private Date arriveDate;

    @ApiModelProperty(value = "规定退房日期")
    private Date leaveDate;

    @ApiModelProperty(value = "规定入住时间")
    private Time arriveTime;

    @ApiModelProperty(value = "规定离开时间")
    private Time leaveTime;

    @ApiModelProperty(value = "实际入住时间")
    private Timestamp realArriveTime;

    @ApiModelProperty(value = "实际离开时间")
    private Timestamp realLeaveTime;

    /**
     * -3：商家取消订单（全额退款）
     * -2：用户取消订单（isCancelStatus）
     * -1：商家未确认（自动全额退款）
     *  0：订单取消（超时未支付）
     *  1：订单已提交（未支付）
     *  2：订单完成支付，等待商家确认（用户可退款）
     *  3：商家已确认（用户可退款）
     *  4：订单完成（用户未取消订单，正常结束；计算分账）
     */
    @ApiModelProperty(value = "订单状态 默认为1")
    private Integer orderStatus;

    /**
     * 0：用户未产生退款请求，默认值
     * 1：取消时间距离入住时间大于等于还有 24 小时，可免费取消预订（全额退款）
     * 2：取消时间距离入住时间小于还有24 小时，首晚房费不可退还（分账记录到order）
     * 3：房客已入住但决定提前退房，取消预订24小时后的未住宿晚数的房费将全额退还
     */
    @ApiModelProperty(value = "订单退款状态 默认为0：未产生退款  1：全额退款 2：首晚退款 3：已入住退款")
    private Integer isCancelStatus;

    @ApiModelProperty(value = "默认为1 用户入住状态 1：未入住 0：已入住")
    private Integer checkStatus;

    @ApiModelProperty(value = "价格")
    private Double price;

    @ApiModelProperty(value = "该订单是否可取消 1：表示可取消 0表示不可取消 默认为0")
    private Integer isCancel;

    @ApiModelProperty(value = "平台获利")
    private Double airbGetPrice;

    @ApiModelProperty(value = "房东获利")
    private Double merchantGetPrice;

    @ApiModelProperty(value = "天数")
    private Integer dayAmount;

    @ApiModelProperty(value = "是否超时 1：未超时 0：已超时")
    private Integer timeOut;

    @ApiModelProperty(value = "超时时长（小时数，不足一个小时按一个小时处理）")
    private Integer overtime;

    @ApiModelProperty(value = "订单备注")
    private String remarks;

    @TableField(exist = false)
    @ApiModelProperty(value = "支付宝订单信息")
    private AliPayBean aliPayBean;

    @TableField(exist = false)
    @ApiModelProperty(value = "房源信息")
    private HolidayHouse holidayHouse;

    @TableField(exist = false)
    @ApiModelProperty(value = "房间信息")
    private House house;

    @TableField(exist = false)
    @ApiModelProperty(value = "房间信息")
    private CancelOrder cancelOrder;

    @TableField(exist = false)
    @ApiModelProperty(value = "评论信息")
    private Comment comment;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户信息")
    private User user;
}
