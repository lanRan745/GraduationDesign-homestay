package com.fjut.server.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_facility")
@ApiModel(value="Facility对象", description="")
public class Facility implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String facilityName;

    @ApiModelProperty(value = "绑定class")
    private String facilityIcon;


}
