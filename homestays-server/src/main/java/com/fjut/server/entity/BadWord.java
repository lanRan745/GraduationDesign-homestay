package com.fjut.server.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 敏感词库
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadWord {
    private String content;
}
