package com.fjut.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_holidayhouse")
@ApiModel(value="Holidayhouse对象", description="")
public class HolidayHouse implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "表id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "房东id")
    private Integer merchantId;

    @ApiModelProperty(value = "时间")
    private Timestamp createTime;

    @ApiModelProperty(value = "地址信息")
    private String address;

    @ApiModelProperty(value = "经度")
    private String lon;

    @ApiModelProperty(value = "维度")
    private String lat;

    @ApiModelProperty(value = "百度检索位置")
    private String bdAddress;

    @ApiModelProperty(value = "概要")
    private String holidayHouseDescribe;

    @ApiModelProperty(value = "详情")
    private String detailDescribe;

    @ApiModelProperty(value = "相关证件图片")
    private String images;

    @TableField(exist = false)
    @ApiModelProperty(value = "图片列表")
    private List<String> allImages;

    @ApiModelProperty(value = "管理员审核状态，默认为1，表示审核中，2表示审核通过，0表示审核未通过")
    private Integer auditStatus;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户信息")
    private User user;
}
