package com.fjut.server.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_house")
@ApiModel(value="House对象", description="")
public class House implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "房间id")
    private Integer id;

    @ApiModelProperty(value = "房东Id")
    private Integer merchantId;

    @ApiModelProperty(value = "房源id")
    private Integer holidayHouseId;

    @ApiModelProperty(value = "规定入住时间")
    private Time arriveTime;

    @ApiModelProperty(value = "规定离开时间")
    private Time leaveTime;

    @ApiModelProperty(value = "价格")
    private Double price;

    @ApiModelProperty(value = "折扣 1- 10")
    private Double discount;

    @ApiModelProperty(value = "订单是否可取消 1可取消 0表示不可取消")
    private Integer isCancel;

    @ApiModelProperty(value = "房间标题")
    private String houseTitle;

    @ApiModelProperty(value = "房间描述")
    private String houseDescribe;

    @TableField(exist = false)
    @ApiModelProperty(value = "该房间的提供的遍历设施")
    private List<Facility> houseFacilities;

    @ApiModelProperty(value = "出租的房源类型 1表示整套 2 表示单间 默认为1")
    private Integer houseType;

    @ApiModelProperty(value = "房间格局描述")
    private String pattern;

    @ApiModelProperty(value = "所有图片字符")
    private String houseImages;

    @TableField(exist = false)
    @ApiModelProperty(value = "房间图片列表")
    private List<String> allDetailImages;

    @ApiModelProperty(value = "1为未营业 2表示营业")
    private Integer businessStatus;

    @TableField(exist = false)
    @ApiModelProperty(value = "所在房源信息")
    private HolidayHouse hh;
}
