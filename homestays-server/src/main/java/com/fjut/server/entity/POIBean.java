package com.fjut.server.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author lanRan
 * @Date 2021/6/1 3:54
 * @Version 1.0
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class POIBean {
    private String province;
    private String city;
    private String district;
    private String street;
    private String address;
    private String lng;
    private String lat;

}
