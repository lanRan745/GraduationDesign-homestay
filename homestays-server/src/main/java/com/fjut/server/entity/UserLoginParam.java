package com.fjut.server.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户登录实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "userLogin对象",description = "")
public class UserLoginParam {

    @ApiModelProperty(value = "用户名",required = true)
    private String username;
    @ApiModelProperty(value = "密码",required = true)
    private String password;
    @ApiModelProperty(value = "图形验证码",required = true)
    private String validationCode;
    @ApiModelProperty(value = "角色：1-管理员 2-商家 3-用户")
    private String role;
    @ApiModelProperty(value = "邮箱",required = true)
    private String email;
}
