package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Order;
import com.fjut.server.service.ICommentService;
import com.fjut.server.service.impl.CommentServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
@RestController
@CrossOrigin
@Api(tags = "CommentController")
public class CommentController {
    @Autowired
    private ICommentService commentService;

    @ApiOperation("获取评论条数")
    @GetMapping("/getCommentAmount")
    public ResponseBean getCommentAmount() {
        return commentService.getCommentAmount();
    }

    @ApiOperation("获取订单评论")
    @GetMapping("/getComments")
    public ResponseBean getComments(@RequestParam(defaultValue = "1")Integer currentPage,
                @RequestParam(defaultValue = "1")Integer size,
                @RequestParam Integer houseId){
        return commentService.getComments(currentPage,size,houseId);
    }

    @PostMapping("/evaluate")
    public ResponseBean evaluate(@RequestParam("rating")Integer rating,
                                 @RequestParam("content")String content,
                                 @RequestParam("orderId")Integer orderId,
                                 @RequestParam("userId")Integer userId,
                                 @RequestParam("houseId")Integer houseId) throws IOException {
        System.out.println(userId);
        System.out.println(houseId);
        return commentService.addComment(rating,content,orderId,userId,houseId);
    }
}
