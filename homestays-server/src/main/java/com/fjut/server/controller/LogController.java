package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.service.ILogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-24
 */
@RestController
@Api(tags = "LogController")
@CrossOrigin
public class LogController {

    @Autowired
    //@Qualifier("logService")
    private ILogService logService;

    @ApiOperation(value = "获得系统信息")
    @GetMapping("/getLogByMerchantId")
    public ResponseBean getLogByMerchantId(){
        return logService.getLogByMerchantId();
    }

    @ApiOperation(value = "消除红点")
    @GetMapping("/clearRed")
    public ResponseBean clearRed(Integer id){
        return logService.clearRed(id);
    }


}
