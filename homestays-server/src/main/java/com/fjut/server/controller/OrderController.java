package com.fjut.server.controller;


import com.alipay.api.AlipayApiException;
import com.fjut.annotation.NotResponseBody;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.AliPayBean;
import com.fjut.server.entity.Order;
import com.fjut.server.entity.User;
import com.fjut.server.mapper.OrderMapper;
import com.fjut.server.mapper.UserMapper;
import com.fjut.server.service.IOrderService;
import com.fjut.server.service.impl.OrderServiceImpl;
import com.fjut.util.AliPayUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-28
 */
@RestController
public class OrderController {

    @Autowired
    private IOrderService orderService;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    /*
     * 用户支付订单
     * 商户订单号 和 商品名称 必有，不能为空，    body：商品描述可为空
     * */
    @NotResponseBody
    @PostMapping("/userOrderPay")
    public ResponseBean userOrderPay(@RequestBody Map<String, String> map, @RequestParam(value = "total_amount") String total_amount) {
        System.out.println(map);
        System.out.println(total_amount);
        return orderService.userOrderPay(map, total_amount);
    }

    @PostMapping("/userCancelOfOrder")
    public ResponseBean userCancelOfOrder(@RequestBody Map<String, String> map, @RequestParam(value = "id") Integer id) {
        return orderService.userCancelOfOrder(map, id);
    }


    @PostMapping("/payOrderAgain")
    public ResponseBean payOrderAgain(@RequestBody AliPayBean aliPayBean, @RequestParam(value = "id") Integer id) {
        System.out.println(aliPayBean);
        return orderService.payOrderAgain(aliPayBean, id);
    }

    @PostMapping("/merchantCancelOrder")
    public ResponseBean merchantCancelOrder(@RequestParam(value = "orderId") Integer orderId,
                                            @RequestParam(value = "remarks") String remarks,
                                            @RequestParam(value = "dayAmount") Integer dayAmount,
                                            @RequestParam(value = "no") String no){
        return orderService.merchantCancelOrder(orderId,remarks,no,dayAmount);
    }

    /**
     * 两个接口：获取 即将开始的旅程的信息
     * orderStatus: 1/2/3
     *
     * @return
     */
    @GetMapping("/getAheadUserOrders")
    public ResponseBean getAheadUserOrders(@RequestParam(defaultValue = "1") Integer currentPage,
                                           @RequestParam(defaultValue = "5") Integer size) {
        return orderService.getAheadUserOrders(currentPage, size);
    }

    @GetMapping("/getCheckOrders")
    public ResponseBean getCheckOrders(@RequestParam(defaultValue = "1") Integer currentPage,
                                       @RequestParam(defaultValue = "5") Integer size) {
        return orderService.getCheckOrders(currentPage, size);
    }

    @GetMapping("/determineOrder")
    public ResponseBean determineOrder(Integer id){
        return orderService.determineOrder(id);
    }

    @GetMapping("/searchCheckOrders")
    public ResponseBean searchCheckOrders(@RequestParam(defaultValue = "1") Integer currentPage,
                                          @RequestParam(defaultValue = "5") Integer size,
                                          @RequestParam(value = "name" ,required = false) String username,
                                          @RequestParam(value = "email",required = false) String email,
                                          @RequestParam(value = "orderStatus",required = false) Integer orderStatus,
                                          @RequestParam(value = "checkStatus",required = false) Integer checkStatus,
                                          @RequestParam(value = "no",required = false) String no){
        return orderService.searchCheckOrders(currentPage, size, username ,email,orderStatus,checkStatus,no);
    }

    @GetMapping("/getOrderStatistics")
    public ResponseBean getOrderStatistics(){
        return orderService.getOrderStatistics();
    }

    @GetMapping("/getMerchantAllOrders")
    public ResponseBean getMerchantAllOrders(@RequestParam(defaultValue = "1") Integer currentPage,
                                             @RequestParam(defaultValue = "5") Integer size,
                                             @RequestParam(value = "name" ,required = false) String username,
                                             @RequestParam(value = "email",required = false) String email,
                                             @RequestParam(value = "orderStatus",required = false) Integer orderStatus,
                                             @RequestParam(value = "no",required = false) String no){
        return orderService.getMerchantAllOrders(currentPage, size, username ,email,orderStatus,no);
    }

    /**
     * 两个接口：获取 过往的旅程的信息
     * orderStatus: -1/0/4
     * @return
     */
    @GetMapping("/getPastUserOrders")
    public ResponseBean getPastUserOrders(@RequestParam(defaultValue = "1")Integer currentPage,
                                          @RequestParam(defaultValue = "5")Integer size){
        return orderService.getPastUserOrders(currentPage,size);
    }

    /*
     * 支付的异步通知函数
     * */
    @PostMapping("notifyurl")
    public void notifyurl(HttpServletRequest request) throws UnsupportedEncodingException {
        System.out.println("==========notifyurl===========");
        //订单号
        String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
        // 交易状态
        String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");
        //交易成功后做出的一些处理
        if(trade_status.equals("TRADE_SUCCESS")){
            //更新订单状态 orderStatus --> 2 参数在xml文件中，没有传入，不要再找了
            Integer orderStatus = orderMapper.selectOrderByNo(out_trade_no).getOrderStatus();
            if(orderStatus == 1) {
                int result = orderMapper.updatePaymentOrder(out_trade_no);
                if (result > 0) {
                    System.out.println("orderMessage：订单成功支付并且成功修改点单状态");
                    Map<String, Object> map = new HashMap<>();

                    //获得订单信息
                    Order order = orderMapper.getNeedRemindOrder(out_trade_no);
                    User user = userMapper.selectById(order.getMerchantId());
                    map.put("order", order);
                    map.put("user", user);
                    //存入redis
                    handleRemindOrders(order.getId());
                    // 发送邮件
                    rabbitTemplate.convertAndSend("mail.remindMerchant", map);
                } else
                    System.out.println("orderMessage：订单已支付，但是未修改订单状态");
            }
        }
    }

    public void handleRemindOrders(Integer id){
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        // 设置过期时间为24小时
        valueOperations.set("remindOrder:" + id, id,30, TimeUnit.SECONDS);
    }


}
