package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.User;
import com.fjut.server.service.IUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-05
 */
@RestController
@CrossOrigin
@Api(tags = "UserController")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("getAllUserInfo")
    public List<User> getAllUserInfo(){
        return userService.getAllUserInfo();
    }

    @GetMapping("/getSingleUserInfo")
    public ResponseBean getSingleUserInfo() {
        return userService.getSingleUserInfo();
    }

    @GetMapping("/getSimpleUserInfo")
    public ResponseBean getSimpleUserInfo(){
        return userService.getSimpleUserInfo();
    }

    @PostMapping("/modifyInfo")
    public ResponseBean modifyInfo(@RequestParam(value = "realname", required = false) String realname,
                                   @RequestParam(value = "idNumber", required = false) String idNumber,
                                   @RequestParam(value = "telephone", required = false) String telephone){
        return userService.modifyInfo(realname,idNumber,telephone);
    }

    @PostMapping("/updateAvatar")
    public ResponseBean updateAvatar(@RequestParam("file") MultipartFile file) throws IOException {
        return userService.updateAvatar(file);
    }

    @GetMapping("/modifyPassword")
    public ResponseBean modifyPassword(@RequestParam("userId")Integer userId,
                                       @RequestParam("newp")String newp,
                                       @RequestParam("oldp")String oldp){
        return userService.userModifyPassword(userId,newp,oldp);
    }

    @PostMapping("/powerSearch")
    public ResponseBean powerSearch(@RequestParam(value = "username",required = false)String username,
                                    @RequestParam(value = "role",required = false)String role,
                                    @RequestParam(value = "currentPage",required = false,defaultValue = "1")Integer currentPage,
                                    @RequestParam(value = "size",required = false, defaultValue = "8")Integer size){
        return userService.powerSearch(username,role,currentPage,size);
    }

    @GetMapping("/ban")
    public ResponseBean ban(@RequestParam("id")Integer id){
        return userService.ban(id);
    }

    @GetMapping("/recovery")
    public ResponseBean recovery(@RequestParam("id")Integer id){
        return userService.recovery(id);
    }

}


