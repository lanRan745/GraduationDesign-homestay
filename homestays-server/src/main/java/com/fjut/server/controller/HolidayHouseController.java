package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.service.IHolidayHouseService;
import com.fjut.server.service.impl.HolidayHouseServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-23
 */

@RestController
@Api(tags = "HolidayHouseController")
@CrossOrigin
public class HolidayHouseController {

    @Autowired
    private IHolidayHouseService holidayHouseService;

    @ApiOperation(value = "添加民宿")
    @PostMapping("/addHomestays")
    public ResponseBean addHolidayHouse (@RequestParam(value ="lng" , required = false)String lng,
                                         @RequestParam(value ="lat" , required = false)String lat,
                                         @RequestParam(value ="bdAddress" , required = false)String bdAddress,
                                         @RequestParam(value ="address" , required = false)String address,
                                         @RequestParam(value ="province" , required = false)String province,
                                         @RequestParam(value ="city" , required = false)String city,
                                         @RequestParam(value ="area" , required = false)String area,
                                         @RequestParam(value ="describe" , required = false)String describe,
                                         @RequestParam(value ="detailDescribe" , required = false)String detailDescribe,
                                         @RequestParam(value = "file", required = false)MultipartFile[] files) throws IOException {
        if(files == null)
            return new ResponseBean().error("上传的图片不能为空");
        else if(files.length < 3)
            return new ResponseBean().error("上传的图片不能小于三张");
        return holidayHouseService.addHolidayHouse(lng,lat,bdAddress,address, province, city, area, describe,detailDescribe, files);
    }

    @ApiOperation(value = "商家概览民宿")
    @GetMapping("/scanHomestays")
    public ResponseBean scanHomestays () {
        return holidayHouseService.scanHomestays();
    }


    /**
     * 管理员审核
     *
     * @auditStatus 新民宿审核状态 1：审核中 0：审核不通过 2：审核通过
     * @return
     */
    @ApiOperation(value = "管理员审核民宿状态")
    @PostMapping("/modifyAuditStatus")
    public ResponseBean modifyAuditStatus (Integer id, Integer auditStatus) {
        return holidayHouseService.modifyAuditStatus(id,auditStatus);
    }

    @ApiOperation(value = "修改房源信息")
    @PostMapping("/modifyHomestays")
    public ResponseBean modifyHomestays(@RequestParam(value ="describe" , required = false)String describe,
                                        @RequestParam(value ="detailDescribe" , required = false)String detailDescribe,
                                        @RequestParam(value ="id" , required = false)String id,
                                        @RequestParam(value ="auditStatus" , required = false)Integer auditStatus,
                                        @RequestParam(value = "file", required = false)MultipartFile[] files) throws IOException {

        if(files != null && files.length < 3)
            return new ResponseBean().error("上传的图片要为三张");
        return holidayHouseService.modifyHomestays(describe, detailDescribe, id, auditStatus, files);
    }

    @ApiOperation("获取当前经纬度附近的房源")
    @PostMapping("/getLongAndLati")
    public ResponseBean getLongAndLati(@RequestParam(value ="lng" , required = false)String lng,
                                       @RequestParam(value ="lat" , required = false)String lat,
                                       @RequestParam(value ="buttonStatus" , required = false)Integer buttonStatus,
                                       @RequestParam(value ="min" , required = false)Integer min,
                                       @RequestParam(value ="max" , required = false)Integer max,
                                       @RequestParam(value ="iscancel" , required = false)Integer iscancel){
        return holidayHouseService.getLongAndLati(lng,lat,buttonStatus,min,max,iscancel);
    }

    @GetMapping("/verifyHolidayHouses")
    public ResponseBean verifyOrders(@RequestParam(defaultValue = "1")Integer currentPage,
                                     @RequestParam(defaultValue = "5")Integer size){

        return holidayHouseService.verifyHolidayHouses(currentPage,size);
    }

    @GetMapping("/passHolidayHouse")
    public ResponseBean passHolidayHouse(@RequestParam("id") Integer id,
                                         @RequestParam("merchantId") Integer merchantId,
                                         @RequestParam("address") String address){
        return holidayHouseService.passHolidayHouse(id,merchantId,address);
    }

    @GetMapping("/rejectHolidayHouse")
    public ResponseBean rejectHolidayHouse(@RequestParam("id") Integer id,
                                           @RequestParam("merchantId") Integer merchantId,
                                           @RequestParam("address") String address,
                                           @RequestParam("remarks") String remark){
        return holidayHouseService.rejectHolidayHouse(id,merchantId,address,remark);
    }

    @GetMapping("/getAdminIndexData")
    public ResponseBean getAdminIndexData(){
        return holidayHouseService.getAdminIndexData();
    }
}
