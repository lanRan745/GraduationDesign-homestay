package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.mapper.FacilityMapper;
import com.fjut.server.service.IFacilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-27
 */
@CrossOrigin
@RestController
@Api(tags = "FacilityController")
public class FacilityController {

    @Autowired
    private IFacilityService iFacilityService;


    @ApiOperation("获取所有便利设施")
    @GetMapping("/getFacilities")
    public ResponseBean getFacilities(){
        return iFacilityService.getFacilities();
    }
}
