package com.fjut.server.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600)
@CrossOrigin()
@RestController
@Api(tags = "TestController")
public class TestController {


    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("username","zzw");
        System.out.println("hello 接口被调用了");
        return "hello";
    }

/*  @ApiOperation(value = "测试邮件发送")
    @GetMapping("/mail")
    public*/
}
