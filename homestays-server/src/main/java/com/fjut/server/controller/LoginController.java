package com.fjut.server.controller;

import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.User;
import com.fjut.server.entity.UserLoginParam;
import com.fjut.server.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * 登录控制类
 */
@Api(tags = "LoginController")
@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private IUserService userService;


    @ApiOperation(value = "租赁用户登录之后返回token")
    @PostMapping("/api/userLogin")
    public ResponseBean userLogin(@RequestBody UserLoginParam userLoginParam, HttpServletRequest request, HttpServletResponse response){
        return userService.login(userLoginParam.getUsername(),userLoginParam.getPassword(),userLoginParam.getValidationCode(),userLoginParam.getRole(),request,response);
    }

    @ApiOperation(value = "修改密码（用户/房东）")
    @PostMapping("/modifyPassword")
    public ResponseBean modifyPassword(@RequestBody UserLoginParam userLoginParam){
        return userService.modifyPassword(userLoginParam.getRole(),userLoginParam.getPassword(),userLoginParam.getEmail());
    }


    /**
     * 之后的具体实现改成 邮箱 比较稳妥
     * @param principal
     * @return
     */
//    @ApiOperation(value = "获取当前登录用户的信息")
//    @GetMapping("/getUserInfo")
//    public User getUserInfo(Principal principal){
//        if (principal == null) {
//            return null;
//        }
//        String username = principal.getName();
//        User user = userService.getUserByUsername(username);
////        String email = principal.getName();
////        User user = userService.getUserBymail(username);
//        user.setPassword(null);
//        return user;
//    }


    /**
     * 前端调用这个接口后，得到200的状态码，直接在前端删除token信息，之后的访问就会被jwt拦截
     * @return
     */
    @ApiOperation(value = "退出登录")
    @PostMapping("/logout")
    public ResponseBean logout(){
        return ResponseBean.success("退出成功");
    }
}
