package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.service.IHouseService;
import com.fjut.server.service.IHousefacilityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-04-26
 */
@RestController
@CrossOrigin
@Api(tags = "HouseController")
public class HouseController {

    @Autowired
    private IHouseService iHouseService;

    @ApiOperation("添加房间")
    @PostMapping("/addNewHouse")
    public ResponseBean addNewHouse(@RequestParam Map<String,Object> map,
                                    @RequestParam("file") MultipartFile[] files) throws IOException {
        return iHouseService.addNewHouse(map,files);
    }

    @ApiOperation("获取该房源下所有房间")
    @GetMapping("/getHouses")
    public ResponseBean getHouses(Integer holidayHouseId){
        return iHouseService.getHouses(holidayHouseId);
    }

    @ApiOperation("删除房间")
    @GetMapping("/deleteHouse")
    public ResponseBean deleteHouse(Integer houseId){
        return iHouseService.deleteHouse(houseId);
    }

    @ApiOperation("修改房间营业状态")
    @GetMapping("/modifyBusinessStatus")
    public ResponseBean modifyBusinessStatus(Integer houseId,Integer businessStatus){
        return iHouseService.modifyBusinessStatus(houseId,businessStatus);
    }

    @ApiOperation("获取点击房源的详细信息")
    @GetMapping("/getHouseDetailMessage")
    public ResponseBean getHouseDetailMessage(Integer houseId) {
        return iHouseService.getHouseDetailMessage(houseId);
    }

    @GetMapping("/getHouseIndexMessage")
    public ResponseBean getHouseIndexMessage(@RequestParam(required = false) String selectCityName) {
        return iHouseService.getHouseIndexMessage(selectCityName);
    }

    @GetMapping("/selectCity")
    public ResponseBean selectCity(@RequestParam String cityName) {
        return iHouseService.selectCity(cityName);
    }

}
