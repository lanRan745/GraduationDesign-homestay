package com.fjut.server.controller;


import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Notice;
import com.fjut.server.service.INoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-06-05
 */
@RestController
public class NoticeController {

    @Autowired
    private INoticeService noticeService;

    @PostMapping("/noticeSearch")
    public ResponseBean noticeSearch(@RequestParam(value = "title",required = false)String title,
                                     @RequestParam(value = "role",required = false)String role,
                                     @RequestParam(value = "currentPage",required = false,defaultValue = "1")Integer currentPage,
                                     @RequestParam(value = "size",required = false, defaultValue = "8")Integer size){
        return noticeService.noticeSearch(title,role,currentPage,size);
    }
    @GetMapping("/deleteNotice")
    public ResponseBean deleteNotice(Integer id){
        return noticeService.deleteNotice(id);
    }

    @PostMapping("/addNotice")
    public ResponseBean addNotice(@RequestBody Notice notice){
        return noticeService.addNotice(notice);
    }

    @GetMapping("/merchantGetNotice")
    public ResponseBean merchantGetNotice(){
        return noticeService.merchantGetNotice();
    }

    @GetMapping("/userGetNotice")
    public ResponseBean userGetNotice(){
        return noticeService.userGetNotice();
    }

}
