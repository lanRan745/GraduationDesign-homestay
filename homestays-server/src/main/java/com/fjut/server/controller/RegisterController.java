package com.fjut.server.controller;

import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.User;
import com.fjut.server.entity.UserLoginParam;
import com.fjut.server.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Author lanRan
 * @Date 2021/4/21 12:02
 * @Version 1.0
 **/
@Api(tags = "RegisterController")
@RestController
@CrossOrigin
public class RegisterController {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private IUserService userService;

    @ApiOperation(value = "发送邮箱验证")
    @PostMapping("/validateMail")
    public ResponseBean validateMail(@RequestParam("email") String email) {

        String code = UUID.randomUUID().toString().substring(25, 31);
        HashMap<String,Object> map = new HashMap<>();
        map.put("email", email);
        map.put("code",code);
        rabbitTemplate.convertAndSend("mail.register",map);
        return new ResponseBean().success("邮件发送成功",code);
    }

    @ApiOperation(value = "发送忘记密码邮箱验证")
    @PostMapping("/validatePasswordMail")
    public ResponseBean validatePasswordMail(@RequestParam("email") String email) {

        String code = UUID.randomUUID().toString().substring(25, 31);
        HashMap<String,Object> map = new HashMap<>();
        map.put("email", email);
        map.put("code",code);
        rabbitTemplate.convertAndSend("mail.modifyPassword",map);
        return new ResponseBean().success("邮件发送成功",code);
    }

    @ApiOperation(value = "用户注册")
    @PostMapping("/register")
    public ResponseBean register(@RequestBody User user) {
        return userService.register(user);
    }

}
