package com.fjut.server.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.Identity;
import com.fjut.server.service.IIdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 郑智伟
 * @since 2021-05-30
 */
@RestController
@CrossOrigin
public class IdentityController {

    @Autowired
    private IIdentityService identityService;

    @PostMapping("/addUserIdentity")
    public ResponseBean addUserIdentity(@RequestParam String username,@RequestParam String idNumber,@RequestParam Integer orderId, @RequestBody Map<String,List<Identity>> array){
        return identityService.addUserIdentity(array.get("array"),orderId,username,idNumber);
    }
}
