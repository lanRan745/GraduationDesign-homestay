package com.fjut.Listener;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fjut.server.entity.*;
import com.fjut.server.mapper.HolidayHouseMapper;
import com.fjut.server.mapper.HouseMapper;
import com.fjut.server.mapper.OrderMapper;
import com.fjut.server.mapper.UserMapper;
import com.fjut.util.LocationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 使用@Scheduled实现定时任务
 * @Author lanRan
 * @Date 2021/5/28 2:55
 * @Version 1.0
 **/
@Component
public class OrderTimer {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HouseMapper houseMapper;
    @Autowired
    private HolidayHouseMapper holidayHouseMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    // 这个方法执行定时任务 表达式表示每天 0点触发，每隔4小时触发一次
    @Transactional
    @Scheduled(cron = "0 0 0/4 * * ?") //cron是一个表达式
    public void execute() throws ParseException {
        //获得现在的时间的时间戳
        System.out.println("--------------定时器正在工作----------------");
        System.out.println(new Date());
        Long now = new Date().getTime();
        System.out.println(now);
        //获取订单的时间 状态为3，商家已确认
        List<Order> checkedOrders = orderMapper.selectList(new QueryWrapper<Order>().eq("orderStatus", 3));

        for(Order order : checkedOrders){
            Date leaveDate = order.getLeaveDate();
            Date leaveTime = order.getLeaveTime();
            String leaveDateTime = leaveDate.toString() + " " + leaveTime.toString();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(leaveDateTime);
            System.out.println(date);
            // 订单结束时间戳
            long orderTime = date.getTime();
            System.out.println(orderTime);
            if(orderTime <= now) {
                //修改订单状态
                order.setOrderStatus(4);
                //瓜分账款
                Double price = order.getPrice();
                //规定两位小数
                DecimalFormat df = new DecimalFormat("#.00");
                Double merchantGetPrice = Double.valueOf(df.format(price*0.9));
                Double airbGetPrice = Double.valueOf(df.format(price*0.1));
                order.setMerchantGetPrice(merchantGetPrice);
                order.setAirbGetPrice(airbGetPrice);
                orderMapper.updateById(order);

                //更新账款
                //更新平台
                User user = userMapper.selectById(49);
                Double up = user.getPrice() + airbGetPrice;
                user.setPrice(up);
                userMapper.updateById(user);

                //更新商家
                User merchant = userMapper.selectById(order.getMerchantId());
                Double mp = merchant.getPrice() + merchantGetPrice;
                merchant.setPrice(mp);
                userMapper.updateById(merchant);
            }
        }
    }

    //24小时更新一次 凌晨两点更新
    @Scheduled(cron = "0 0 2/24 * * ?") //cron是一个表达式
    public void getHotCities() throws ParseException {
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();

            List<Map<String,List<House>>> retrievalResult = new ArrayList<>();
            //获得所有订单
            List<Order> orders = orderMapper.selectList(null);

            for(Order order : orders){

                House house = fromRedisGetHouse(order.getHouseId());
                //房若间状态为停业，直接下一个订单
                if(house.getBusinessStatus() == 1) continue;
                System.out.println(order);
                HolidayHouse holidayHouse = fromRedisGetHolidayHouse(order.getHolidayHouseId());
                //获得经纬度
                String lng = holidayHouse.getLon();
                String lat = holidayHouse.getLat();
                //获得城市信息
                POIBean poiBean = LocationUtil.getPOIBean(lng, lat);
                if (poiBean == null) {
                    System.out.println("POIBEAN == NULL");
                    continue;
                }
                String city = poiBean.getCity();

                if(retrievalResult.size() == 0){

                    List<House> l = new ArrayList<>();
                    Map<String,List<House>> map = new HashMap<>();
                    l.add(house);
                    map.put(city, l);
                    retrievalResult.add(map);
                    continue;
                }

                Boolean flagcity = false;
                a : for(Map<String,List<House>> map : retrievalResult){

                    for (Map.Entry<String, List<House>> entry : map.entrySet()) {
                        String mapKey = entry.getKey();
                        List<House> mapValue = entry.getValue();
                        if(mapKey.equals(city)) {
                            flagcity = true;
                            break;
                        }
                    }
                    if(flagcity == true){
                        List<House> l = map.get(city);
                        int i = 0;
                        for(House h : l){
                            i++;
                            if(h.getId() == house.getId())
                                break a;
                            if (h.getId() != house.getId() && l.size() == i){
                                l.add(house);
                                break a;
                            }
                        }
                    }
                    else {
                        List<House> l = new ArrayList<>();
                        Map<String,List<House>> m = new HashMap<>();
                        l.add(house);
                        m.put(city, l);
                        retrievalResult.add(m);
                        break a;
                    }

                }

            }
            //存入redis，设定过期时间为48小时
            valueOperations.set("hotCities", retrievalResult,20, TimeUnit.SECONDS);
    }

    House fromRedisGetHouse (Integer houseId){
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        House house =  (House) valueOperations.get("house-" + houseId);
        // 没有的话从数据库中找
        if(house == null){
            house = houseMapper.selectOne(new QueryWrapper<House>().eq("id", houseId));
            //存入redis，设定过期时间为48小时
            valueOperations.set("house-" + houseId, house,30, TimeUnit.SECONDS);
        }
        return house;
    }

    HolidayHouse fromRedisGetHolidayHouse (Integer holidayHouseId){
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        HolidayHouse holidayHouse =  (HolidayHouse) valueOperations.get("HolidayHouse-" + holidayHouseId);
        // 没有的话从数据库中找
        if(holidayHouse == null){
            holidayHouse = holidayHouseMapper.selectOne(new QueryWrapper<HolidayHouse>().eq("id", holidayHouseId));
            //存入redis，设定过期时间为48小时
            valueOperations.set("HolidayHouse-" + holidayHouseId, holidayHouse,30, TimeUnit.SECONDS);
        }
        return holidayHouse;
    }


}

