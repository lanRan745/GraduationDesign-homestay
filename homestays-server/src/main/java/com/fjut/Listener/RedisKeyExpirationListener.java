package com.fjut.Listener;

import com.fjut.server.entity.Order;
import com.fjut.server.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

/**
 * @Author lanRan
 * @Date 2021/5/26 22:15
 * @Version 1.0
 **/
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private OrderMapper orderMapper;


    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    /**
     * 处理失效订单
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        //message.toString() 可以获取失效的key
        String expiredKey = message.toString();
        if(expiredKey.startsWith("pendingOrder")) {
            String[] orderId = expiredKey.split(":");
            System.out.println(expiredKey + "过期键值监听器：===========》" + orderId[1]);
            int orderStatus = orderMapper.selectById(orderId[1]).getOrderStatus();
            //状态码为1 表示超时还未支付，此时处理成字符超时
            if(orderStatus == 1) {
                Order r = new Order();
                r.setId(Integer.valueOf(orderId[1]));
                r.setOrderStatus(0);
                orderMapper.updateById(r);
            }
        }
        else if(expiredKey.startsWith("remindOrder")) {
            System.out.println("ceshisfsfjsdojsdojsdfjsofjosjfosjfosjfosid");
            String[] orderId = expiredKey.split(":");
            System.out.println(expiredKey + "过期键值监听器：===========>" + orderId[1]);
            int orderStatus = orderMapper.selectById(orderId[1]).getOrderStatus();
            //状态码为1 表示超时还未支付，此时处理成字符超时
            if(orderStatus == 2) {
                Order r = new Order();
                r.setId(Integer.valueOf(orderId[1]));
                // -1 表示商家未确认订单
                r.setOrderStatus(-1);
                orderMapper.updateById(r);
            }
        }
    }
}
