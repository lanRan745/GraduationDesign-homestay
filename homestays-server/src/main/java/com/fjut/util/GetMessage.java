package com.fjut.util;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fjut.handler.ResponseBean;
import com.fjut.server.entity.User;
import com.fjut.server.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @Author lanRan
 * @Date 2021/5/10 18:38
 * @Version 1.0
 **/
@Component
public class GetMessage {
    @Autowired
    private  UserMapper userMapper;
    @Autowired
    private  RedisTemplate redisTemplate;

    // 获得商家
    public  User getCurrentUser() {
        String currentUserName = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        // 先从数据库获取
        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        User user =  (User) valueOperations.get("user-" + currentUserName);

        // 没有的话从数据库中找
        if(user == null){
            user = userMapper.selectOne(new QueryWrapper<User>().eq("username", currentUserName));
            if(user == null)
                new ResponseBean().error("发生错误，当前用户不存在");
            //存入redis
            valueOperations.set("user-" + currentUserName, user);
        }
        return user;
    }
}
