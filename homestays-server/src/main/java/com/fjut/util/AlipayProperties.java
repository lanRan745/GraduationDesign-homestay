package com.fjut.util;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author lanRan
 * @Date 2021/5/23 14:27
 * @Version 1.0
 **/

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix="alipay")
public class AlipayProperties {
    private String APP_ID = "appId"; //应用ID
    // 用户私钥
    private String PRIVARY_KEY = "privateKey";
    //支付宝公钥
    private String PUBLIC_KEY = "publicKey";
    //服务器异步通知页面路径 ,需要公网能访问到
    private String NOTIFY_URL = "notifyUrl";
    //支付宝成功跳转页面  页面跳转同步通知页面路径 需要公网能访问到。
    private String RETURN_URL = "returnUrl";
    //签名方式
    private String SIGN_TYPE = "signType";
    //字符编码格式
    private String CHARSET = "charset";
    //支付宝网关
    private String GATEWAY_URL = "gatewayUrl";
    //日志地址
    private String LOG_PATH = "logPath";
}
