package com.fjut.util;

import com.fjut.server.entity.BadWord;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 敏感词检测工具
 * */

public class BadWordsUtil {
    /**
     * 敏感词库
     */
    public static Map sensitiveWordMap = null;

    /**
     * 只过滤最小敏感词
     */
    public static int minMatchTYpe = 1;

    /**
     * 过滤所有敏感词
     */
    public static int maxMatchType = 2;


    /**
     * 敏感词库
     * @return
     */
    public static List<BadWord> initBadWordList() throws IOException {
        // 构建敏感词库
        List<BadWord> badwordLisat = new ArrayList<>();

        File file = new File("homestays-server/myBadWords.txt");
        InputStreamReader inputReader = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
        BufferedReader bf = new BufferedReader(inputReader);
        // 按行读取字符串
        String str;
        while ((str = bf.readLine()) != null) {
            BadWord badWord = new BadWord();
            badWord.setContent(str);
            badwordLisat.add(badWord);
        }
        return badwordLisat;
    }



    /**
     * 敏感词库敏感词数量
     * @return
     */
    public int getWordSize()
    {
        if (BadWordsUtil.sensitiveWordMap == null)
        {
            return 0;
        }
        return BadWordsUtil.sensitiveWordMap.size();
    }

    /**
     * 是否包含敏感词
     *
     * @param txt
     * @param matchType
     * @return
     */
    public static boolean isContaintSensitiveWord(String txt, int matchType)
    {
        boolean flag = false;
        for (int i = 0; i < txt.length(); i++)
        {
            int matchFlag = checkSensitiveWord(txt, i, matchType);
            if (matchFlag > 0)
            {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 获取敏感词内容
     *
     * @param txt
     * @param matchType
     * @return 敏感词内容
     */
    public static Set<String> getSensitiveWord(String txt, int matchType)
    {
        Set<String> sensitiveWordList = new HashSet<String>();

        for (int i = 0; i < txt.length(); i++)
        {
            int length = checkSensitiveWord(txt, i, matchType);
            if (length > 0)
            {
                // 将检测出的敏感词保存到集合中
                sensitiveWordList.add(txt.substring(i, i + length));
                i = i + length - 1;
            }
        }

        return sensitiveWordList;
    }

    /**
     * 替换敏感词
     *
     * @param txt
     * @param matchType
     * @param replaceChar
     * @return
     */
    public static String replaceSensitiveWord(String txt, int matchType, String replaceChar)
    {
        String resultTxt = txt;
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        String word = null;
        String replaceString = null;
        while (iterator.hasNext())
        {
            word = iterator.next();
            replaceString = getReplaceChars(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }

        return resultTxt;
    }

    /**
     * 替换敏感词内容
     *
     * @param replaceChar
     * @param length
     * @return
     */
    private static String getReplaceChars(String replaceChar, int length) {
        return "";
//        return replaceChar + replaceChar.repeat(Math.max(0, length - 1));
    }

    /**
     * 检查敏感词数量
     *
     * @param txt
     * @param beginIndex
     * @param matchType
     * @return
     */
    public static int checkSensitiveWord(String txt, int beginIndex, int matchType)
    {
        boolean flag = false;
        // 记录敏感词数量
        int matchFlag = 0;
        char word = 0;
        Map nowMap = BadWordsUtil.sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++)
        {
            word = txt.charAt(i);
            // 判断该字是否存在于敏感词库中
            nowMap = (Map) nowMap.get(word);
            if (nowMap != null)
            {
                matchFlag++;
                // 判断是否是敏感词的结尾字，如果是结尾字则判断是否继续检测
                if ("1".equals(nowMap.get("isEnd")))
                {
                    flag = true;
                    // 判断过滤类型，如果是小过滤则跳出循环，否则继续循环
                    if (BadWordsUtil.minMatchTYpe == matchType)
                    {
                        break;
                    }
                }
            }
            else
            {
                break;
            }
        }
        if (!flag)
        {
            matchFlag = 0;
        }
        return matchFlag;
    }

    /**
     * 敏感词检测
     * */
    public static boolean isContaintSensitiveWord(List<BadWord> list,String message){
        boolean boo = false;
        try {
            // 初始化敏感词库对象
            BadWordsInit sensitiveWordInit = new BadWordsInit();
            // 传入SensitivewordEngine类中的敏感词库
            BadWordsUtil.sensitiveWordMap = sensitiveWordInit.initKeyWord(list);
            boo = BadWordsUtil.isContaintSensitiveWord(message, 2);
            System.out.println(boo);
        } catch (Exception e) {
            System.out.println("发现敏感词");
        }
        return boo;
    }
}
