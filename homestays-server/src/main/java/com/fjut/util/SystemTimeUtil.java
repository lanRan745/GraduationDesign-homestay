package com.fjut.util;

import java.sql.Timestamp;

/**
 * @Author lanRan
 * @Date 2021/4/26 15:46
 * @Version 1.0
 **/
public class SystemTimeUtil {
    public static Timestamp getCurrentTime(){

        long time = System.currentTimeMillis();
        return new Timestamp(time);
    }
}
