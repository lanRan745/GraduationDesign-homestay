package com.fjut.util;

import com.fjut.server.entity.POIBean;
import net.sf.json.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.reflections.util.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author lanRan
 * @Date 2021/6/1 3:00
 * @Version 1.0
 **/
public class LocationUtil {
    public static Map<String,Object> getLocationMessage(String lon,String lat){
        Map<String, Object> map = null;
        String url = "http://api.map.baidu.com/reverse_geocoding/v3/?ak=E9cvC39bhOdQGuLw0CBdnx68HOIUNnTH&output=json&coordtype=wgs84ll&location="+lat+","+lon;
        try {
            HttpClient client = HttpClientBuilder.create().build();//构建一个Client
            HttpGet get = new HttpGet(url.toString());    //构建一个GET请求
            HttpResponse response = client.execute(get);//提交GET请求
            HttpEntity result = response.getEntity();//拿到返回的HttpResponse的"实体"
            String content = EntityUtils.toString(result);

            if (Utils.isEmpty(content)) {
                return null;
            }
            JSONObject res = JSONObject.fromObject(content);


            map = JsonUtil.parseJSON2Map(res); //通过下面的函数将json转化为map
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取地址失败");
        }
        return map;
    }

    public static POIBean getPOIBean(String lon,String lat){

        POIBean poiBean = new POIBean();
        Map <String, Object> result = LocationUtil.getLocationMessage(lon,lat);
        if (result == null) {
            return null;
        }
        Map<String, Object> r = JsonUtil.parseJSON2Map((JSONObject) result.get("result"));
        Map<String, Object> addressComponent = JsonUtil.parseJSON2Map((JSONObject) r.get("addressComponent"));

        poiBean.setAddress((String) r.get("formatted_address"));
        poiBean.setProvince((String) addressComponent.get("province"));
        poiBean.setCity((String) addressComponent.get("city"));
        poiBean.setDistrict((String) addressComponent.get("district"));
        poiBean.setStreet((String) addressComponent.get("street"));
        poiBean.setLng(lon);
        poiBean.setLat(lat);

        return poiBean;
    }
}
