package com.fjut.util;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.fjut.server.entity.AliPayBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author lanRan
 * @Date 2021/5/23 14:24
 * @Version 1.0
 **/
@Component
public class AliPayUtil {

    @Autowired
    private AlipayProperties aliPayProperties;

    public String pay(AliPayBean data) throws AlipayApiException {
        String app_id = aliPayProperties.getAPP_ID();
        String charset = aliPayProperties.getCHARSET();
        String gateway_url = aliPayProperties.getGATEWAY_URL();
        String notify_url = aliPayProperties.getNOTIFY_URL();
        String privary_key = aliPayProperties.getPRIVARY_KEY();
        String public_key = aliPayProperties.getPUBLIC_KEY();
        String return_url = aliPayProperties.getRETURN_URL();
        String sign_type = aliPayProperties.getSIGN_TYPE();
        String format = "json";
        //创建服务端，用于生成签名吧
        AlipayClient alipayClient = new DefaultAlipayClient(gateway_url, app_id,
        privary_key, format, charset, public_key, sign_type);
        //设置请求的参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        //设置同步和异步通知的页面路径
        alipayRequest.setNotifyUrl(notify_url);
        alipayRequest.setReturnUrl(return_url);
        //设置参数
        alipayRequest.setBizContent(JSON.toJSONString(data));
        //发送付款请求
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        System.out.println(result);
        return result;
    }

    public Boolean refund(String refund_amount, String out_trade_no){
        String app_id = aliPayProperties.getAPP_ID();
        String charset = aliPayProperties.getCHARSET();
        String gateway_url = aliPayProperties.getGATEWAY_URL();
        String notify_url = aliPayProperties.getNOTIFY_URL();
        String privary_key = aliPayProperties.getPRIVARY_KEY();
        String public_key = aliPayProperties.getPUBLIC_KEY();
        String return_url = aliPayProperties.getRETURN_URL();
        String sign_type = aliPayProperties.getSIGN_TYPE();
        String format = "json";
        //创建服务端，用于生成签名吧
        // 表示支持部分退款
        String out_request_no = "HZ01RF001";
        AlipayClient alipayClient = new DefaultAlipayClient(gateway_url, app_id,
                privary_key, format, charset, public_key, sign_type);
        AlipayTradeRefundRequest req = new AlipayTradeRefundRequest();
        System.out.println("refund_amount:" + refund_amount + " out_trade_no:" + out_trade_no);

        req.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\"," + "\"refund_amount\":\"" + refund_amount + "\","
                + "\"out_request_no\":\"" + out_request_no + "\"}");
        AlipayTradeRefundResponse response = null;
        try {
            response = alipayClient.execute(req);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        System.out.println(response.getBody());

        if(response.isSuccess()){
            System.out.println("退款接口响应体："+response.getBody());
            System.out.println("返回参数信息："+response.getBody());
            return true;
        }
        else{
            System.out.println("退款失败");
            return false;
        }
    }
}
