package 发布_订阅队列;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * 发布/订阅-消息生产者
 * @Author lanRan
 * @Date 2021/4/20 19:39
 * @Version 1.0
 **/
public class Send {
    // 定义交换机名称
    private final static String EXCHANGE_NAME = "exchange_direct";
    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);

        try (
            // 连接工厂创建连接
            // Connection继承了 AutoClosable接口，会自动关闭
            Connection connection = factory.newConnection();
            // 创建信道
            Channel channel = connection.createChannel()) {

            // 绑定交换机 路由模式（FANOUT 是广播模式）
            channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
            String info = "info message";
            String INFO_Routing_key = "INFO";

            String error = "error message";
            String ERROR_Routing_key = "ERROR";

            String warning = "warning message";
            String WARNING_Routing_key = "WARNING";
            // 发送消息
            channel.basicPublish(EXCHANGE_NAME,INFO_Routing_key,null,info.getBytes(StandardCharsets.UTF_8));
            channel.basicPublish(EXCHANGE_NAME,ERROR_Routing_key,null,error.getBytes(StandardCharsets.UTF_8));
            channel.basicPublish(EXCHANGE_NAME,WARNING_Routing_key,null,warning.getBytes(StandardCharsets.UTF_8));
            System.out.println("[x] Sent" + info + "'");
            System.out.println("[x] Sent" + error + "'");
            System.out.println("[x] Sent" + warning + "'");
        }
    }
}
