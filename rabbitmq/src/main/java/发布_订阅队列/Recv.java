package 发布_订阅队列;

import com.rabbitmq.client.*;

/**
 * 发布/订阅-消息消费者 所有的消费者都可以收到同一条消息
 * @Author lanRan
 * @Date 2021/4/20 20:58
 * @Version 1.0
 **/
public class Recv {
    // 定义队列名称
    private final static String EXCHANGE_NAME = "exchange_direct";

    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);

        // 连接工厂创建连接
        Connection connection = factory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 绑定交换机是为了绑定队列，队列由交换机创建，并且是排他队列，
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 获取队列
        String quequeName = channel.queueDeclare().getQueue();

        String INFO_Routing_key = "INFO";
        String ERROR_Routing_key = "ERROR";
        String WARNING_Routing_key = "WARNING";
       // 绑定队列和交换机
        channel.queueBind(quequeName, EXCHANGE_NAME, ERROR_Routing_key);
        System.out.println("[*] Waiting for messages. To exit press CTRL+C");

        // delivery获取到的消息
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
        };
        //监听队列消费信息
        channel.basicConsume(quequeName, true,deliverCallback,consumerTag -> {});

    }
}
