package 确认模式;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * 确认模式_消息生产者
 * @Author lanRan
 * @Date 2021/4/20 19:39
 * @Version 1.0
 **/
public class Send {
    // 队列名称
    private final static String QUEUE_NAME = "confirm_sync";
    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);
        Connection connection = null;
        Channel channel = null;
        try{
            // 连接工厂创建连接
            // Connection继承了 AutoClosable接口，会自动关闭
            connection = factory.newConnection();
            // 创建信道
            channel = connection.createChannel();
            // 开启确认模式
            channel.confirmSelect();

            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            String message = "Hello World1";
            // 发送消息
            /**
             * exchange：交换机，默认为空
             * props：额外参数
             */
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes(StandardCharsets.UTF_8));
           //普通confirm模式 发一次返回一次
//            if(channel.waitForConfirms())
//                System.out.println("消息发送成功！");
            //批量confirm模式 只要有一条没发送成功，就抛出异常
            channel.waitForConfirmsOrDie();

            System.out.println("[x] Sent" + message + "'");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
