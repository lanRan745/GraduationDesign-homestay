package 确认模式.异步模式;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeoutException;

/**
 * 信道确认模式-异步-生产者
 * @Author lanRan
 * @Date 2021/4/21 2:18
 * @Version 1.0
 **/
public class Send {
    private static final String QUEUE_NAME = "confirm_async";

    public static void main(String[] args) {
        // 定义连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);
        Connection connection = null;
        Channel channel = null;

        try {
            //维护信息发送绘制deliveryTag
            final SortedSet<Long> confirmSet = Collections.synchronizedSortedSet(new TreeSet());
            connection = factory.newConnection();
            channel = connection.createChannel();

            //开启confirm确认模式
            channel.confirmSelect();
            //声明队列
            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            //添加channel监听
            channel.addConfirmListener(new ConfirmListener() {
                //已确认
                @Override
                public void handleAck(long deliveryTag, boolean multiple) throws IOException {
                    if(multiple) { //多条
                        System.out.println("handleAck --success-->multiple" + deliveryTag);
                        //清除 deliveryTag 项标识id
                        confirmSet.headSet(deliveryTag + 1L).clear();
                    } else { //单条
                        System.out.println("handleAck--success-->single" + deliveryTag);
                        confirmSet.remove(deliveryTag);
                    }
                }
                //未确认
                @Override
                public void handleNack(long deliveryTag, boolean multiple) throws IOException {
                    if(multiple) { //多条
                        System.out.println("handleNack --failed-->multiple" + deliveryTag);
                        //清除 deliveryTag 项标识id
                        confirmSet.headSet(deliveryTag + 1L).clear();
                    } else { //单条
                        System.out.println("handleNack --failed-->single" + deliveryTag);
                        confirmSet.remove(deliveryTag);
                    }
                }
            });

            while(true) {
                //创建消息
                String message = "confirm异步模式";
                // 获取unconfirm的消息序号deliveryTag
                Long seqNo = channel.getNextPublishSeqNo();
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes("utf-8"));
                //将消息序号添加至SortedSet
                confirmSet.add(seqNo);
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            //关闭通道
            if(channel != null && channel.isOpen()) {
                try {
                    channel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null && connection.isOpen()) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
