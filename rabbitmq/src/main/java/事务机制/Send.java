package 事务机制;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * 事务-简单队列消息生产者
 * @Author lanRan
 * @Date 2021/4/20 19:39
 * @Version 1.0
 **/
public class Send {
    // 队列名称
    private final static String QUEUE_NAME = "tx";
    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);
        Connection connection = null;
        Channel channel = null;
        try{
            // 连接工厂创建连接
            // Connection继承了 AutoClosable接口，会自动关闭
            connection = factory.newConnection();
            // 创建信道
            channel = connection.createChannel();
            /**
             * 绑定队列
             * durable：是否持久化
             * exclusive：排他队列
             *      1、基于连接可见，对于其他连接是不可见当前连接的队列，普通队列可以看到
             *      2、不同连接的排他队列名不能相同
             *      3、排他队列会在连接关闭自动删除，即使配置了持久化，也仍然会参数这个队列
             *      4、false表示不是排他队列
             * autoDelete：自动删除
             * arguments：参数
             */

            // 开启一个事务
            channel.txSelect();

            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            String message = "Hello World1";
            // 发送消息
            /**
             * exchange：交换机，默认为空
             * props：额外参数
             */
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes(StandardCharsets.UTF_8));
           // int i = 1/0;
            // 提交事务
            channel.txCommit();
            System.out.println("[x] Sent" + message + "'");
        }catch (Exception e){
            e.printStackTrace();
            // 回滚事务
            channel.txRollback();
        }
    }
}
