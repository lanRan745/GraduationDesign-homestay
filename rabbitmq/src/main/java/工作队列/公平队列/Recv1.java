package 工作队列.公平队列;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * 工作队列-公平-消息消费者 ：能者多劳
 * 轮询队列缺陷：如果有新的消费队列，不能够消费队列中的消息
 * @Author lanRan
 * @Date 2021/4/20 20:58
 * @Version 1.0
 **/
public class Recv1 {
    // 定义队列名称
    private final static String QUEUE_NAME = "work_fair";

    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);

        // 连接工厂创建连接
        Connection connection = factory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 绑定队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        // 公平队列配置
        int prefetchCount = 1;
        // 限制消费者每次只能接受一条消息，处理完才能接受下一条消息
        channel.basicQos(prefetchCount);

        System.out.println("[*] Waiting for messages. To exit press CTRL+C");

        // delivery获取到的消息
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // 模拟消费耗时
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            /**
             * 设置轮询队列
             * 手动确认
             * multiple：是否确认多条
             */
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        };
        //监听队列消费信息 autoAck 自动回执
        channel.basicConsume(QUEUE_NAME, true,deliverCallback,consumerTag -> {});

    }
}
