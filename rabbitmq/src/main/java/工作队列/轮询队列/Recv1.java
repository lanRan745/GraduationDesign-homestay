package 工作队列.轮询队列;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * 工作队列-轮询-消息消费者 ：为了解决生产者的生产能力远远大于消费者的消费能力，所以创建多个消费者
 * @Author lanRan
 * @Date 2021/4/20 20:58
 * @Version 1.0
 **/
public class Recv1 {
    // 定义队列名称
    private final static String QUEUE_NAME = "work_rr";

    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);

        // 连接工厂创建连接
        Connection connection = factory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 绑定队列
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        System.out.println("[*] Waiting for messages. To exit press CTRL+C");

        // delivery获取到的消息
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            // 模拟消费耗时
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
            /**
             * 设置轮询队列
             * 手动确认
             * multiple：是否确认多条
             */
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
        };
        //监听队列消费信息 autoAck 自动回执
        channel.basicConsume(QUEUE_NAME, true,deliverCallback,consumerTag -> {});

    }
}
