package 工作队列.轮询队列;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

/**
 * 工作队列-轮询-消息生产者
 * @Author lanRan
 * @Date 2021/4/20 19:39
 * @Version 1.0
 **/
public class Send {
    // 队列名称
    private final static String QUEUE_NAME = "work_rr";
    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);

        try (
            Connection connection = factory.newConnection();
            // 创建信道
            Channel channel = connection.createChannel()) {

            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            for (int i = 0; i < 20; i++) {
                String message = "Hello World" + i;
                channel.basicPublish("",QUEUE_NAME,null,message.getBytes(StandardCharsets.UTF_8));
                System.out.println("[x] Sent" + message + "'" + i);
            }

        }
    }
}
