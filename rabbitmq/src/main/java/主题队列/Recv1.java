package 主题队列;

import com.rabbitmq.client.*;

/**
 * 主题队列-消息消费者
 * @Author lanRan
 * @Date 2021/4/20 20:58
 * @Version 1.0
 **/
public class Recv1 {
    // 定义队列名称
    private final static String EXCHANGE_NAME = "exchange_topic";

    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("8.131.55.144");
        factory.setUsername("admin");
        factory.setVirtualHost("/");
        factory.setPassword("admin");
        factory.setPort(5672);

        // 连接工厂创建连接
        Connection connection = factory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 绑定交换机是为了绑定队列，队列由交换机创建，并且是排他队列，
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        // 获取队列
        String quequeName = channel.queueDeclare().getQueue();

/*        String INFO_Routing_key = "info.message.orange";
        String ERROR_Routing_key = "error.rabbit.lazy";
        String WARNING_Routing_key = "orange.warning.message";*/
        // 使用通配符 #表示通配0个或多个 .表示通配一个
        String Routing_key = ".message.";
       // 绑定队列和交换机
        channel.queueBind(quequeName, EXCHANGE_NAME,Routing_key);
        System.out.println("[*] Waiting for messages. To exit press CTRL+C");

        // delivery获取到的消息
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message + "'");
        };
        //监听队列消费信息
        channel.basicConsume(quequeName, true,deliverCallback,consumerTag -> {});

    }
}
