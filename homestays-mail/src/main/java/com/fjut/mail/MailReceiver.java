package com.fjut.mail;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;

/**
 * @Author lanRan
 * @Date 2021/4/21 3:16
 * @Version 1.0
 **/
@Component
public class MailReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailReceiver.class);

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private MailProperties mailProperties;
    @Autowired
    private TemplateEngine templateEngine;

    @RabbitListener(queues = "mail.register")
    public void handler(String email,String code){
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg);
        System.out.println(code);
        try{
            //发件人
            helper.setFrom(mailProperties.getUsername());
            //收件人
            helper.setTo(email);
            //主题
            helper.setSubject("邮箱验证");
            //发送日期
            helper.setSentDate(new Date());
            //内容
            Context context = new Context();
            //邮件内容
            context.setVariable("name", "郑智伟");
            context.setVariable("code", code);
            //将mail.html文件和值设置进去
            String mail = templateEngine.process("mail", context);
            helper.setText(mail);
            //发送邮件
            javaMailSender.send(msg);
        } catch (MessagingException e) {
            e.printStackTrace();
            LOGGER.error("邮件发送失败",e.getMessage());
        }


    }
}
